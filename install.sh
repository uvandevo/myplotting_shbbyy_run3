# Create environment
export ENV="venv"
export ENV_DIR=$PWD/$ENV
python3 -m venv $ENV
source $ENV_DIR/bin/activate

export PYTHONPATH=$ENV_DIR/lib/python3.9/site-packages/:$PWD/:$PYTHONPATH
#export PATH=$PWD/scripts/:$PATH

# # Install packages
# echo "Installing"
# python3 -m pip install --upgrade pip
# python3 -m pip install -r requirements.txt