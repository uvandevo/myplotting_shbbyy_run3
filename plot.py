import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = False
import os, glob
import math
import numpy as np
import pandas as pd
from variablesDicts import *
from myUtils import *
from processList import *

ROOT.gInterpreter.ProcessLine('''
                float dphi(float phi1, float phi2) {
                    float t_dPhi = abs(phi1-phi2);
                    if (t_dPhi > TMath::Pi()) {t_dPhi = abs(t_dPhi - 2*TMath::Pi());}
                    return t_dPhi;
                }
                ''')

def plotmyyComparison():
    # Get map of samples to files
    sampleMap = SampleMap()
    campaignMap = CampaignMap()
    # Get legend info for signals and backgrounds
    samplestoPlot = SampleValidationDict()
    # Get plot info for different variables to plot
    kinVarPlotInfo = KinematicVarPlotDict()
    # Get the variables to define columns in the rdfs, and cuts to filter the rdfs, for plotting
    variables = VariablesDict()
    selections = SelectionDict()
    samplesExtraPathMxAOD = ['ggFHyy', 'ttHyy', 'yy+jets']

    for sample in samplesToValidate:
        for camp in campaignToValidate:
            # Get the MxAOD root file
            fName = sampleMap[sample]['file_name_mxaod']
            rtag = campaignMap['mc16{}'.format(camp)]
            ptag = sampleMap[sample]['p_tag_mxaod']
            path_MxAOD = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027yybb_fix/mc16{}/Nominal/mc16{}.{}_{}_{}_h027yybb_fix.root'.format(camp,camp,fName,rtag,ptag)
            if sample in samplesExtraPathMxAOD: path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
            if sample in 'VBFHyy' and (camp in 'd' or camp in 'e'): path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
            if sample in 'qqZHyy' and camp in 'e': path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)

            # Get the Easyjet ntuple root file
            if 'mX' in sample:
                fileName = sampleMap[sample]['file_name_easyjet']
                did = sampleMap[sample]['did']
                tags = campaignMap['sig mc20{}'.format(camp)]
                path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/Easyjet_{}_mc20{}_{}.root'.format(did,camp,fileName)
                f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(fileName, tags))
            else:
                folderName = sampleMap[sample]['folder_path_easyjet']
                tags = campaignMap['bkg mc20{}'.format(camp)]
                fileName = sampleMap[sample]['file_nmr_{}'.format(camp)]
                path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/user.uvandevo.{}_{}_TREE/user.uvandevo.{}.output-tree.root'.format(folderName,tags,fileName)
                f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(sample, tags))

            # Create a ROOT dataframe for each file
            df = {}
            f_Easyjet = ROOT.TFile.Open(path_Easyjet)
            main_tree = f_Easyjet.Get("AnalysisMiniTree")
            friend_tree = f_weightEasyjet.Get("AnalysisMiniTree")
            main_tree.AddFriend(friend_tree)
            df['Easyjet'] = ROOT.RDataFrame(main_tree)

            f_MxAOD = ROOT.TFile.Open(path_MxAOD)
            df['MxAOD'] = ROOT.RDataFrame("CollectionTree", path_MxAOD)
            files = list(df.keys())

            # Define the columns of the rdf for each variable (not needed for cutflow though)
            for variable in variables:
                #print(variables[var])
                ROOT.gInterpreter.ProcessLine('''
                float dphi(float phi1, float phi2) {
                    float t_dPhi = abs(phi1-phi2);
                    if (t_dPhi > TMath::Pi()) {t_dPhi = abs(t_dPhi - 2*TMath::Pi());}
                    return t_dPhi;
                }
                ''')
                df['Easyjet'] = df['Easyjet'].Define(variable, variables[variable]['easyjet'])
                df['MxAOD'] = df['MxAOD'].Define(variable, variables[variable]['mxaod'])

            # Get sum of weights for MxAOD and "SFYieldMxAOD" which is lumi/sumOfWeights
            SFYieldMxAOD, sum_of_weights_noDalitz_MxAOD = getYieldFactors_mxaod(f_MxAOD, camp)

            # Define columns for the weight
            df['Easyjet'] = df['Easyjet'].Define('total_weight', 'weight')
            df['MxAOD'] = df['MxAOD'].Define('total_weight', 'HGamEventInfoAuxDyn.crossSectionBRfilterEff * HGamEventInfoAuxDyn.weight * HGamEventInfoAuxDyn.yybb_weight * HGamEventInfoAuxDyn.weightFJvt * {}'.format(SFYieldMxAOD))


            for selection in selectionToPlot:
                # Do the selection
                df_tmp = {}
                df_tmp['Easyjet'] = df['Easyjet'].Filter(selections[selection]['easyjet'])
                df_tmp['MxAOD'] = df['MxAOD'].Filter(selections[selection]['mxaod'])
                histos = {}
                for var in ['myy_inv_mass', 'myy_hardestVertex_inv_mass', 'myy_RatioPVs_inv_mass']:
                    # Get variables and hist info
                    varColumnRDF = kinVarPlotInfo[var]['variable']
                    n_bins = kinVarPlotInfo[var]['nBins']
                    xmin = kinVarPlotInfo[var]['xMin']
                    xmax = kinVarPlotInfo[var]['xMax']
                    unit = kinVarPlotInfo[var]['units']
                    binwidth = (xmax-xmin)/n_bins
                    xlabel = kinVarPlotInfo[var]['x-axis title']
                    ylabel = 'Fraction of events/{} {}'.format(binwidth, unit)
                    if 'RatioPVs' in var:
                        n_bins2 = n_bins
                        xlabel2 = xlabel
                        xmin2 = xmin
                        xmax2 = xmax
                        ylabel = 'Fraction of events/{}'.format(round(binwidth, 4))
                    else:
                        n_bins1 = n_bins
                        xlabel1 = xlabel
                        xmin1 = xmin
                        xmax1 = xmax
                    # Book histogram
                    hists = histmaker(df_tmp, files, "", xlabel, ylabel, n_bins, xmin, xmax, varColumnRDF)
                    histos[var] = hists['MxAOD'].Clone()

                # Get plot info
                extraLabelProcess = samplestoPlot[sample]['extra label']
                extraLabelCampaign = "MxAOD mc16{}".format(camp)
                selectionLabel = selections[selection]['legend upper']
                path_plots = './plots_myy_240503/'
                #path_plots = '/eos/user/u/uvandevo/SHbbyyRun3/plots_kinVarVal_240320/'
                fig_title = "{} mc16{} m_yy comparison {}".format(sample, camp, selectionLabel)
                fig_format = "png"

                plotDiphotonInvMassComparison(histos, n_bins1, xlabel1, xmin1, xmax1, n_bins2, xlabel2, xmin2, xmax2, extraLabelProcess, extraLabelCampaign, selectionLabel, path_plots, fig_title, fig_format)
                npy = df_tmp['MxAOD'].AsNumpy(columns=['m_yy_ratioPVs'])
                # for i in npy['m_yy_ratioPVs']:
                #     print(i)

def ValidationEasyjetMxAOD():
    # Get map of samples to files
    sampleMap = SampleMap()
    campaignMap = CampaignMap()
    # Get legend info for signals and backgrounds
    samplestoPlot = SampleValidationDict()
    # Get plot info for different variables to plot
    kinVarPlotInfo = KinematicVarPlotDict()
    # Get the variables to define columns in the rdfs, and cuts to filter the rdfs, for plotting
    variables = VariablesDict()
    selections = SelectionDict()
    samplesExtraPathMxAOD = ['ggFHyy', 'ttHyy', 'yy+jets']

    for sample in samplesToValidate:
        for camp in campaignToValidate:
            # Get the MxAOD root file
            fName = sampleMap[sample]['file_name_mxaod']
            rtag = campaignMap['mc16{}'.format(camp)]
            ptag = sampleMap[sample]['p_tag_mxaod']
            path_MxAOD = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027yybb_fix/mc16{}/Nominal/mc16{}.{}_{}_{}_h027yybb_fix.root'.format(camp,camp,fName,rtag,ptag)
            if sample in samplesExtraPathMxAOD: path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
            if sample in 'VBFHyy' and (camp in 'd' or camp in 'e'): path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
            if sample in 'qqZHyy' and camp in 'e': path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)

            # Get the Easyjet ntuple root file
            if 'mX' in sample:
                fileName = sampleMap[sample]['file_name_easyjet']
                did = sampleMap[sample]['did']
                tags = campaignMap['sig mc20{}'.format(camp)]
                path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/Easyjet_{}_mc20{}_{}.root'.format(did,camp,fileName)
                f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(fileName, tags))
            else:
                folderName = sampleMap[sample]['folder_path_easyjet']
                tags = campaignMap['bkg mc20{}'.format(camp)]
                fileName = sampleMap[sample]['file_nmr_{}'.format(camp)]
                path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/user.uvandevo.{}_{}_TREE/user.uvandevo.{}.output-tree.root'.format(folderName,tags,fileName)
                f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(sample, tags))

            # Create a ROOT dataframe for each file
            df = {}
            f_Easyjet = ROOT.TFile.Open(path_Easyjet)
            main_tree = f_Easyjet.Get("AnalysisMiniTree")
            friend_tree = f_weightEasyjet.Get("AnalysisMiniTree")
            main_tree.AddFriend(friend_tree)
            df['Easyjet'] = ROOT.RDataFrame(main_tree)

            f_MxAOD = ROOT.TFile.Open(path_MxAOD)
            df['MxAOD'] = ROOT.RDataFrame("CollectionTree", path_MxAOD)
            files = list(df.keys())

            # Define the columns of the rdf for each variable (not needed for cutflow though)
            for variable in variables:
                #print(variables[var])
                ROOT.gInterpreter.ProcessLine('''
                float dphi(float phi1, float phi2) {
                    float t_dPhi = abs(phi1-phi2);
                    if (t_dPhi > TMath::Pi()) {t_dPhi = abs(t_dPhi - 2*TMath::Pi());}
                    return t_dPhi;
                }
                ''')

                df['Easyjet'] = df['Easyjet'].Define(variable, variables[variable]['easyjet'])
                df['MxAOD'] = df['MxAOD'].Define(variable, variables[variable]['mxaod'])

            # Get sum of weights for MxAOD and "SFYieldMxAOD" which is lumi/sumOfWeights
            SFYieldMxAOD, sum_of_weights_noDalitz_MxAOD = getYieldFactors_mxaod(f_MxAOD, camp)

            # Define columns for the weight, for the Easyjet ntuples the column 'weight' is from the post process tree and includes (AMIXsection * 1000 * Luminosity * FilterEff * kFactor * mcEventWeights) / sumOfWeights
            df['Easyjet'] = df['Easyjet'].Define('total_weight', 'weight')
            df['MxAOD'] = df['MxAOD'].Define('total_weight', 'HGamEventInfoAuxDyn.crossSectionBRfilterEff * HGamEventInfoAuxDyn.weight * HGamEventInfoAuxDyn.yybb_weight * HGamEventInfoAuxDyn.weightFJvt * {}'.format(SFYieldMxAOD))

            ## For data
            # df['Easyjet'] = df['Easyjet'].Define('weight', '1')
            # df['MxAOD'] = df['MxAOD'].Define('weight', '1')

            for var in kinVarToPlot:
                for selection in selectionToPlot:
                    # Do the selection
                    df_tmp = {}
                    df_tmp['Easyjet'] = df['Easyjet'].Filter(selections[selection]['easyjet'])
                    df_tmp['MxAOD'] = df['MxAOD'].Filter(selections[selection]['mxaod'])

                    # Get variables and hist info
                    varColumnRDF = kinVarPlotInfo[var]['variable']
                    n_bins = kinVarPlotInfo[var]['nBins']
                    xmin = kinVarPlotInfo[var]['xMin']
                    xmax = kinVarPlotInfo[var]['xMax']
                    unit = kinVarPlotInfo[var]['units']
                    binwidth = (xmax-xmin)/n_bins
                    xlabel = kinVarPlotInfo[var]['x-axis title']
                    ylabel = 'Fraction of events/{} {}'.format(binwidth, unit)
                    #ylabel = 'Events/{} {}'.format(binwidth, unit)
                    # Book histogram
                    hists = histmaker(df_tmp, files, "", xlabel, ylabel, n_bins, xmin, xmax, varColumnRDF)

                    # Get plot info
                    extraLabelProcess = samplestoPlot[sample]['extra label']
                    extraLabelCampaign = "mc16/20{}".format(camp)
                    selectionLabel = selections[selection]['legend upper']
                    path_plots = './plots_myy_240503/'
                    #path_plots = '/eos/user/u/uvandevo/SHbbyyRun3/plots_kinVarVal_240320/'
                    fig_title = "{} mc16_20{} {} {}".format(sample, camp, var, selectionLabel)
                    fig_format = "png"
                    # Plot variable
                    plotValPlots(hists, n_bins, xlabel, xmin, xmax, extraLabelProcess, extraLabelCampaign, selectionLabel, path_plots, fig_title, fig_format)
                    # path_plots = './plots/'
                    # plotValPlots(hists, n_bins, xlabel, xmin, xmax, extraLabelProcess, extraLabelCampaign, selectionLabel, path_plots, fig_title, fig_format)

def ValidationDataMC():
    # Get map of samples to files
    sampleMap = SampleMap()
    campaignMap = CampaignMap()
    dataCampaignMap = DataCampaignMap()
    # Get legend info and color info for merged samples
    samplestoPlotMC = SampleDataMCDict()
    # Get plot info for different variables to plot
    kinVarPlotInfo = KinematicVarPlotDict()
    # Get the variables to define columns in the rdfs, and cuts to filter the rdfs, for plotting
    variables = VariablesDict()
    selections = SelectionDictSkimmedSamples()
    campaigns = ['a', 'd', 'e']
    years = ['15', '16', '17', '18']

    for selection in selectionToPlot:
        for var in histosToPlotMCData:
            # Create empty dict to store histograms for data and samples
            hists = {}
            # Create a ROOT dataframe for each file
            df_tmp = {}
            for year in years:
                path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/merged_ntuples/user.uvandevo.SH_data_v2.data{}_13TeV.periodAllYear.grp{}_v01_p5855_TREE.root'.format(year, year)

                f_Easyjet = ROOT.TFile.Open(path_Easyjet)
                df_tmp[year] = ROOT.RDataFrame("AnalysisMiniTree", f_Easyjet)

                # Define the columns of the rdf for each variable
                for variable in variables:
                    df_tmp[year] = df_tmp[year].Define(variable, variables[variable]['easyjet'])

                # Define columns for the weight
                df_tmp[year] = df_tmp[year].Define('total_weight', '1.')

                # Blind data
                df_tmp[year] = df_tmp[year].Filter('m_yy < 120 || m_yy > 130')
                
                # Do the selection
                df_tmp[year] = df_tmp[year].Filter(selections[selection]['easyjet'])

            
            # Get variables and hist info
            varColumnRDF = kinVarPlotInfo[var]['variable']
            n_bins = kinVarPlotInfo[var]['nBins']
            xmin = kinVarPlotInfo[var]['xMin']
            xmax = kinVarPlotInfo[var]['xMax']
            unit = kinVarPlotInfo[var]['units']
            binwidth = (xmax-xmin)/n_bins
            xlabel = kinVarPlotInfo[var]['x-axis title']
            ylabel = 'Events/{} {}'.format(binwidth, unit)

            # Book histogram
            hists_years = histmaker(df_tmp, years, "", xlabel, ylabel, n_bins, xmin, xmax, varColumnRDF)
            h_temp = hists_years['15'].GetValue()
            h_temp.Add(hists_years['16'].GetValue())
            h_temp.Add(hists_years['17'].GetValue())
            h_temp.Add(hists_years['18'].GetValue())
            hists['data'] = h_temp
            h_temp.Delete()

            # Create empty list to store histograms for the samples
            hists_samples = {}
            for sample in samplesToStack:
                # Create a ROOT dataframe for each file
                df_tmp = {}
                for camp in campaigns:
                    if 'mX' in sample:
                        fileName = sampleMap[sample]['file_name_easyjet']
                        did = sampleMap[sample]['did']
                        tags = campaignMap['sig mc20{}'.format(camp)]
                        path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/Easyjet_{}_mc20{}_{}.root'.format(did,camp,fileName)
                        f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/merged_ntuples/{}_{}_PostProcessed_ntuple.root".format(fileName, tags))
                    else:
                        folderName = sampleMap[sample]['folder_path_easyjet']
                        tags = campaignMap['bkg mc20{}'.format(camp)]
                        fileName = sampleMap[sample]['file_nmr_{}'.format(camp)]
                        path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/user.uvandevo.{}_{}_TREE/user.uvandevo.{}.output-tree.root'.format(folderName,tags,fileName)
                        f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/merged_ntuples/{}_{}_PostProcessed_ntuple.root".format(sample, tags))


                    f_Easyjet = ROOT.TFile.Open(path_Easyjet)
                    main_tree = f_Easyjet.Get("AnalysisMiniTree")
                    friend_tree = f_weightEasyjet.Get("AnalysisMiniTree")
                    main_tree.AddFriend(friend_tree)
                    df_tmp[camp] = ROOT.RDataFrame(main_tree)

                    # Define the columns of the rdf for each variable
                    for variable in variables:
                        df_tmp[camp] = df_tmp[camp].Define(variable, variables[variable]['easyjet'])

                    df_tmp[camp] = df_tmp[camp].Define('total_weight', 'weight')
                    
                    # Do the selection
                    df_tmp[camp] = df_tmp[camp].Filter(selections[selection]['easyjet'])

                # Get variables and hist info
                varColumnRDF = kinVarPlotInfo[var]['variable']
                n_bins = kinVarPlotInfo[var]['nBins']
                xmin = kinVarPlotInfo[var]['xMin']
                xmax = kinVarPlotInfo[var]['xMax']
                unit = kinVarPlotInfo[var]['units']
                binwidth = (xmax-xmin)/n_bins
                xlabel = kinVarPlotInfo[var]['x-axis title']
                ylabel = 'Events/{} {}'.format(binwidth, unit)
                # Book histogram
                hists_camp = histmaker(df_tmp, campaigns, "", xlabel, ylabel, n_bins, xmin, xmax, varColumnRDF)
                h_temp = hists_camp['a'].GetValue()
                h_temp.Add(hists_camp['d'].GetValue())
                h_temp.Add(hists_camp['e'].GetValue())
                hists_samples[sample] = h_temp
                h_temp.Delete()
            
            # for i, s in enumerate(['ggFHHbbyy', 'VBFHHbbyy']):
            #     if i == 0: h_temp = hists_samples[s]
            #     else: h_temp.Add(hists_samples[s])
            # hists['HH'] = h_temp
            # h_temp.Delete()

            # # Add samples in the for loop list when we have the missing samples
            # for i, s in enumerate(['ggFHyy', 'VBFHyy', 'W+Hyy', 'W-Hyy', 'qqZHyy', 'ggZHyy', 'ttHyy']):
            #     if i == 0: h_temp = hists_samples[s]
            #     else: h_temp.Add(hists_samples[s])
            # hists['Hyy'] = h_temp
            # h_temp.Delete()

            hists['yy_jets'] = hists_samples['yy+jets']

            # # Add for loop when we have the missing samples
            # hists['ttyy'] = hists_samples['ttyy_noallhad']

            # Add histogram for Z(qq)yy when we have the missing samples

            # Get plot info
            selectionLabel = selections[selection]['legend upper']
            #path_plots = './plots/'
            path_plots = ''
            fig_title = "DataMC {} {}".format(var, selectionLabel)
            fig_format = "png"
            xmin = kinVarPlotInfo[var]['xMin']
            xmax = kinVarPlotInfo[var]['xMax']
            unit = kinVarPlotInfo[var]['units']
            binwidth = (xmax-xmin)/n_bins
            ylabel = 'Events/{} {}'.format(binwidth, unit)
            # Plot variable
            plotDataMCPlots(hists, n_bins, xlabel, ylabel, xmin, xmax, selectionLabel, path_plots, fig_title, fig_format)



if __name__ == "__main__":
    ValidationEasyjetMxAOD()
    #ValidationDataMC()
    #generatePostProcessFiles()
    #plotmyyComparison()