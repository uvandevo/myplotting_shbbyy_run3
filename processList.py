
# Selections to plot
selectionToPlot = [
    #'no_selection',
    #'AtLeast_2jets',
    #'LessThan_6jets'
    #'diphoton_mass',
    'Exactly_2bjets',
]

# Kinematic variables to plot for the MxAOD vs Easyjet validation
kinVarToPlot = [
    'Leading_photon_pt',
    'Subleading_photon_pt',
    'Leading_photon_e',
    'Subleading_photon_e',
    'Leading_photon_eta',
    'Subleading_photon_eta',
    'Leading_photon_phi',
    'Subleading_photon_phi',
    'bjet1_pt',
    'bjet2_pt',
    'bjet1_eta',
    'bjet2_eta',
    'bjet1_phi',
    'bjet2_phi',
    'dPhi_yy',
    'dR_yy',
    'dPhi_bb',
    'myy_inv_mass',
    'mbb_inv_mass',
    'mbbyy_inv_mass',
    'myy_hardestVertex_inv_mass'
]

# Variables to plot for MC/Data comparison
histosToPlotMCData = [
    #'Leading_photon_pt',
    # 'Subleading_photon_pt',
    # 'Leading_photon_e',
    # 'Subleading_photon_e',
    #'bjet1_pt',
    # 'bjet2_pt',
    'myy_inv_mass',
    #'mbb_inv_mass',
    # 'mbbyy_inv_mass',
]

# Samples to stack in MC/Data plots
samplesToStack = [
    # #HH
    # 'ggFHHbbyy',
    # 'VBFHHbbyy',
    # #single Higgs
    # 'ggFHyy',
    # 'VBFHyy',
    # 'W+Hyy',
    # 'W-Hyy',
    # 'qqZHyy',
    # 'ggZHyy',
    # 'ttHyy',
    #yy+jets    
    #'yy+jets',
    #ttyy
    #'ttyy_allhad', #missing
    #'ttyy_noallhad',
    #Z(qq)yy
    #'Zqq_yy',  #missing
    #'Zbb_yy',  #missing
]

# Samples to validate
samplesToValidate = [
    ## Signals
    'mX_750_mS_400',
    'mX_750_mS_170',
    'mX_245_mS_090',
    'mX_205_mS_030',
    'mX_170_mS_030',
    ## Backgrounds
    # #HH
    #'ggFHHbbyy',
    # 'VBFHHbbyy',
    # #single Higgs
    #'ggFHyy',
    # 'VBFHyy',
    # 'W+Hyy',
    # 'W-Hyy',
    # 'qqZHyy',
    # 'ggZHyy',
    # 'ttHyy',
    # #yy+jets    
    #'yy+jets',
    # #ttyy
    # #'ttyy_allhad', #missing
    #'ttyy_noallhad',
    # #Z(qq)yy
    # #'Zqq_yy',  #missing
    # #'Zbb_yy',  #missing
]

# Campaign to validate
campaignToValidate = [
    'a',
    'd',
    'e'
]