import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = False
import os, glob
import math
import numpy as np
from variablesDicts import *
from myUtils import *
from processList import *

# Get map of samples to files
sampleMap = SampleMap()
campaignMap = CampaignMap()
# Get legend info for signals and backgrounds
samplestoPlot = SampleValidationDict()
# Get plot info for different variables to plot
kinVarPlotInfo = KinematicVarPlotDict()
# Get the variables to define columns in the rdfs, and cuts to filter the rdfs, for cutflow
variables = VariablesDict()
cuts = SelectionDict()
samplesExtraPathMxAOD = ['ggFHyy', 'ttHyy', 'yy+jets']

for sample in samplesToValidate:
    for camp in campaignToValidate:
        # Get the MxAOD root file
        fName = sampleMap[sample]['file_name_mxaod']
        rtag = campaignMap['mc16{}'.format(camp)]
        ptag = sampleMap[sample]['p_tag_mxaod']
        path_MxAOD = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h027yybb_fix/mc16{}/Nominal/mc16{}.{}_{}_{}_h027yybb_fix.root'.format(camp,camp,fName,rtag,ptag)
        if sample in samplesExtraPathMxAOD: path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
        if sample in 'VBFHyy' and (camp in 'd' or camp in 'e'): path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)
        if sample in 'qqZHyy' and camp in 'e': path_MxAOD = path_MxAOD + '/mc16{}.{}_{}_{}_h027yybb_fix.001.root'.format(camp,fName,rtag,ptag)

        # Get the Easyjet ntuple root file
        if 'mX' in sample:
            fileName = sampleMap[sample]['file_name_easyjet']
            did = sampleMap[sample]['did']
            tags = campaignMap['sig mc20{}'.format(camp)]
            path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/merged_ntuples/Easyjet_{}_mc20{}_{}.root'.format(did,camp,fileName)
            #f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(fileName, tags))
        else:
            folderName = sampleMap[sample]['folder_path_easyjet']
            tags = campaignMap['bkg mc20{}'.format(camp)]
            fileName = sampleMap[sample]['file_nmr_{}'.format(camp)]
            path_Easyjet = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/user.uvandevo.{}_{}_TREE/user.uvandevo.{}.output-tree.root'.format(folderName,tags,fileName)
            #f_weightEasyjet = ROOT.TFile.Open("/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(sample, tags))

        # Create a ROOT dataframe for each file
        df = {}
        f_Easyjet = ROOT.TFile.Open(path_Easyjet)
        main_tree = f_Easyjet.Get("AnalysisMiniTree")
        # friend_tree = f_weightEasyjet.Get("AnalysisMiniTree")
        # main_tree.AddFriend(friend_tree)
        df['Easyjet'] = ROOT.RDataFrame(main_tree)

        f_MxAOD = ROOT.TFile.Open(path_MxAOD)
        df['MxAOD'] = ROOT.RDataFrame("CollectionTree", path_MxAOD)
        files = list(df.keys())

        # Define the columns of the rdf for each variable (not needed for cutflow though)
        for var in variables:
            ROOT.gInterpreter.ProcessLine('''
                float dphi(float phi1, float phi2) {
                    float t_dPhi = abs(phi1-phi2);
                    if (t_dPhi > TMath::Pi()) {t_dPhi = abs(t_dPhi - 2*TMath::Pi());}
                    return t_dPhi;
                }
                ''')
            
            #print(variables[var])
            df['Easyjet'] = df['Easyjet'].Define(var, variables[var]['easyjet'])
            df['MxAOD'] = df['MxAOD'].Define(var, variables[var]['mxaod'])

        histos_names = f_Easyjet.GetListOfKeys()
        hist_name = [k.GetName() for k in f_Easyjet.GetListOfKeys() if "CutBookkeeper_" in k.GetName() and "_NOSYS" in k.GetName()]
        histoSumOfWeights = f_Easyjet.Get(hist_name[0])

        nTotalEasyjet = histoSumOfWeights.GetBinContent(1)
        nTotalWeightedEasyjet = histoSumOfWeights.GetBinContent(2)

        histos_names = f_MxAOD.GetListOfKeys()
        hist_name = [k.GetName() for k in f_MxAOD.GetListOfKeys() if "_noDalitz_weighted" in k.GetName()]
        #print(len(hist_name))
        hist = f_MxAOD.Get(hist_name[0])

        nTotalGenEventsMxAOD = hist.GetBinContent(1)    #Number of generated events in MxAOD
        nFilteredGenEventsMxAOD = hist.GetBinContent(2)    #Number of generated events in DxAOD, some backgrounds may have skimmed events
        sum_of_weights_noDalitz_MxAOD = hist.GetBinContent(3)
        # Rescale sum of weights in case the sample has filtered events between MxAOD and DxAOD
        nTotalWeightedMxAOD = (nTotalGenEventsMxAOD/nFilteredGenEventsMxAOD)*sum_of_weights_noDalitz_MxAOD
        nTotalWeightedMxAOD = sum_of_weights_noDalitz_MxAOD

        if camp == 'a': lumi = 36646.74 #pb-1
        if camp == 'd': lumi = 44630.6  #pb-1
        if camp == 'e': lumi = 58791.6  #pb-1
        
        
        # Define columns for the weight
        df['Easyjet'] = df['Easyjet'].Define('mc_weight', 'generatorWeight_NOSYS')
        df['MxAOD'] = df['MxAOD'].Define('mc_weight', 'HGamEventInfoAuxDyn.weightInitial')\
                                    .Define('yield', 'HGamEventInfoAuxDyn.crossSectionBRfilterEff * HGamEventInfoAuxDyn.weight * HGamEventInfoAuxDyn.yybb_weight * HGamEventInfoAuxDyn.weightFJvt * {}'.format(lumi/sum_of_weights_noDalitz_MxAOD))

        print("Easyjet sumofweights histo ", nTotalWeightedEasyjet)
        print("Easyjet sumofweights sum ", df['Easyjet'].Sum('mc_weight').GetValue())
        print("mxaod sumofweights histo", nTotalWeightedMxAOD)
        print("mxaod sumofweights sum", df['MxAOD'].Sum('mc_weight').GetValue())

        nTotalWeightedMxAOD = df['MxAOD'].Sum('yield').GetValue()


        # Define empty list/dict with cuts and efficiencies
        cutflowDictEasyjet = {}
        cutflowDictMxAOD = {}

        cutList = list(cuts)

        print(cutList)


        # For loop to fill cut flow and rel eff
        for i, cut in enumerate(cuts):
            # Get values for Easyjet
            df_easyjet_tmp = df['Easyjet'].Filter(cuts[cut]['easyjet'])
            nPassEasyjet = df_easyjet_tmp.Count().GetValue()
            nPassWeightedEasyjet = df_easyjet_tmp.Sum('mc_weight').GetValue()
            
            # Get the efficiency of the cut for standard cut flow
            cutEfficiency, cutLowerError, cutUpperError = getEfficiency(nTotalWeightedEasyjet, nPassWeightedEasyjet)

            # Get the relative efficiency
            if i > 1: relEfficiency, relLowerError, relUpperError = getEfficiency(cutflowDictEasyjet[cutList[i-1]]['nPass'], nPassWeightedEasyjet)
            else: relEfficiency, relLowerError, relUpperError = getEfficiency(nTotalWeightedEasyjet, nPassWeightedEasyjet)
            
            # Fill dict with all the values
            cutflowDictEasyjet[cut] = {'nPass': nPassWeightedEasyjet, 'cutflow eff': cutEfficiency, 'cutflow error up': cutUpperError, 'cutflow error low': cutLowerError,\
                                    'rel eff': relEfficiency, 'rel error up': relUpperError, 'rel error low': relLowerError}

            # Get values for MxAOD
            df_mxaod_tmp = df['MxAOD'].Filter(cuts[cut]['mxaod'])
            nPassMxAOD = df_mxaod_tmp.Count().GetValue()
            nPassWeightedMxAOD = df_mxaod_tmp.Sum('yield').GetValue()

            # Get the efficiency of the cut for standard cut flow
            cutEfficiency, cutLowerError, cutUpperError = getEfficiency(nTotalWeightedMxAOD, nPassWeightedMxAOD)
            
            # Get the relative efficiency
            if i > 1: relEfficiency, relLowerError, relUpperError = getEfficiency(cutflowDictMxAOD[cutList[i-1]]['nPass'], nPassWeightedMxAOD)
            else: relEfficiency, relLowerError, relUpperError = getEfficiency(nTotalWeightedMxAOD, nPassWeightedMxAOD)
            
            # Fill dict with all the values
            cutflowDictMxAOD[cut] = {'nPass': nPassWeightedMxAOD, 'cutflow eff': cutEfficiency, 'cutflow error up': cutUpperError, 'cutflow error low': cutLowerError,\
                                    'rel eff': relEfficiency, 'rel error up': relUpperError, 'rel error low': relLowerError}
            
        # Print function for yield, cutflow and rel eff
        print("\nSample and campaign: ", sample, " ", camp)
        # print("Yield in MxAOD before selection: ", round(cutflowDictMxAOD['no_selection']['yield'], 2))
        # print("Yield in MxAOD after trigger cut: ", round(cutflowDictMxAOD['trigger']['yield'], 2))
        # print("Yield in MxAOD after two tight id + iso photons: ", round(cutflowDictMxAOD['photon_2tightIDIS']['yield'], 2))
        # print("Yield in MxAOD after pass rel pt: ", round(cutflowDictMxAOD['rel_pt']['yield'], 2))
        # print("Yield in MxAOD after diphoton invariant mass: ", round(cutflowDictMxAOD['diphoton_mass']['yield'], 2))
        # print("Yield in MxAOD after 0 leptons cut: ", round(cutflowDictMxAOD['0lep']['yield'], 2))
        # print("Yield in MxAOD after >= 2 jets cut: ", round(cutflowDictMxAOD['AtLeast_2jets']['yield'], 2))
        # print("Yield in MxAOD after < 6 jets cut: ", round(cutflowDictMxAOD['LessThan_6jets']['yield'], 2))
        # print("Yield in MxAOD after exactly 2 b-jets (DL1r 77% WP) cut: ", round(cutflowDictMxAOD['Exactly_2bjets']['yield'], 2))

        # print("\nYield in Easyjet before selection: ", round(cutflowDictEasyjet['no_selection']['yield'], 2))
        # print("Yield in Easyjet after trigger cut: ", round(cutflowDictEasyjet['trigger']['yield'], 2))
        # print("Yield in Easyjet after two tight id + iso photons: ", round(cutflowDictEasyjet['photon_2tightIDIS']['yield'], 2))
        # print("Yield in Easyjet after pass rel pt: ", round(cutflowDictEasyjet['rel_pt']['yield'], 2))
        # print("Yield in Easyjet after diphoton invariant mass: ", round(cutflowDictEasyjet['diphoton_mass']['yield'], 2))
        # print("Yield in Easyjet after 0 leptons cut: ", round(cutflowDictEasyjet['0lep']['yield'], 2))
        # print("Yield in Easyjet after >= 2 jets cut: ", round(cutflowDictEasyjet['AtLeast_2jets']['yield'], 2))
        # print("Yield in Easyjet after < 6 jets cut: ", round(cutflowDictEasyjet['LessThan_6jets']['yield'], 2))
        # print("Yield in Easyjet after exactly 2 b-jets (DL1d 77% WP) cut: ", round(cutflowDictEasyjet['Exactly_2bjets']['yield'], 2))

        print("\nEfficiency in MxAOD after trigger cut: ", round(cutflowDictMxAOD['trigger']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['trigger']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['trigger']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after two tight id + iso photons: ", round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after pass rel pt: ", round(cutflowDictMxAOD['rel_pt']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['rel_pt']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['rel_pt']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after diphoton invariant mass: ", round(cutflowDictMxAOD['diphoton_mass']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['diphoton_mass']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['diphoton_mass']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after 0 leptons cut: ", round(cutflowDictMxAOD['0lep']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['0lep']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['0lep']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after >= 2 jets cut: ", round(cutflowDictMxAOD['AtLeast_2jets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['AtLeast_2jets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['AtLeast_2jets']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after < 6 jets cut: ", round(cutflowDictMxAOD['LessThan_6jets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['LessThan_6jets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['LessThan_6jets']['cutflow error low']*100, 2), ") %")
        print("Efficiency in MxAOD after exactly 2 b-jets (DL1r 77 WP%) cut: ", round(cutflowDictMxAOD['Exactly_2bjets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['Exactly_2bjets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['Exactly_2bjets']['cutflow error low']*100, 2), ") %")

        print("\nEfficiency in Easyjet after trigger cut: ", round(cutflowDictEasyjet['trigger']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['trigger']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['trigger']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after two tight id + iso photons: ", round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after pass rel pt: ", round(cutflowDictEasyjet['rel_pt']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['rel_pt']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['rel_pt']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after diphoton invariant mass: ", round(cutflowDictEasyjet['diphoton_mass']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['diphoton_mass']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['diphoton_mass']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after 0 leptons cut: ", round(cutflowDictEasyjet['0lep']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['0lep']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['0lep']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after >= 2 jets cut: ", round(cutflowDictEasyjet['AtLeast_2jets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['AtLeast_2jets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['AtLeast_2jets']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after < 6 jets cut: ", round(cutflowDictEasyjet['LessThan_6jets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['LessThan_6jets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['LessThan_6jets']['cutflow error low']*100, 2), ") %")
        print("Efficiency in Easyjet after exactly 2 b-jets (DL1d 77% WP) cut: ", round(cutflowDictEasyjet['Exactly_2bjets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['Exactly_2bjets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['Exactly_2bjets']['cutflow error low']*100, 2), ") %")

        print("\nREL Efficiency in MxAOD before selection: ", round(nTotalWeightedMxAOD/nTotalWeightedMxAOD*100, 2), " ")
        print("REL Efficiency in MxAOD after trigger cut: ", round(cutflowDictMxAOD['trigger']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['trigger']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['trigger']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after two tight id + iso photons: ", round(cutflowDictMxAOD['photon_2tightIDIS']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['photon_2tightIDIS']['rel error up']*100, 2), " , -", round(cutflowDictMxAOD['photon_2tightIDIS']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after pass rel pt: ", round(cutflowDictMxAOD['rel_pt']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['rel_pt']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['rel_pt']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after diphoton invariant mass: ", round(cutflowDictMxAOD['diphoton_mass']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['diphoton_mass']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['diphoton_mass']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after 0 leptons cut: ", round(cutflowDictMxAOD['0lep']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['0lep']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['0lep']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after >= 2 jets cut: ", round(cutflowDictMxAOD['AtLeast_2jets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['AtLeast_2jets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['AtLeast_2jets']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after < 6 jets cut: ", round(cutflowDictMxAOD['LessThan_6jets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['LessThan_6jets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['LessThan_6jets']['rel error low']*100, 2), ") %")
        print("REL Efficiency in MxAOD after exactly 2 b-jets (DL1r 77% WP) cut: ", round(cutflowDictMxAOD['Exactly_2bjets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['Exactly_2bjets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['Exactly_2bjets']['rel error low']*100, 2), ") %")

        print("\nREL Efficiency in Easyjet before selection: ", round(nTotalWeightedEasyjet/nTotalWeightedEasyjet*100, 2), " ")
        print("REL Efficiency in Easyjet after trigger cut: ", round(cutflowDictEasyjet['trigger']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['trigger']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['trigger']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after two tight id + iso photons: ", round(cutflowDictEasyjet['photon_2tightIDIS']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['photon_2tightIDIS']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['photon_2tightIDIS']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after pass rel pt: ", round(cutflowDictEasyjet['rel_pt']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['rel_pt']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['rel_pt']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after diphoton invariant mass: ", round(cutflowDictEasyjet['diphoton_mass']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['diphoton_mass']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['diphoton_mass']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after 0 leptons cut: ", round(cutflowDictEasyjet['0lep']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['0lep']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['0lep']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after >= 2 jets cut: ", round(cutflowDictEasyjet['AtLeast_2jets']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['AtLeast_2jets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['AtLeast_2jets']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after < 6 jets cut: ", round(cutflowDictEasyjet['LessThan_6jets']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['LessThan_6jets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['LessThan_6jets']['rel error low']*100, 2), ") %")
        print("REL Efficiency in Easyjet after exactly 2 b-jets (DL1d 77% WP) cut: ", round(cutflowDictEasyjet['Exactly_2bjets']['rel eff']*100, 2), " (+", round(cutflowDictEasyjet['Exactly_2bjets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['Exactly_2bjets']['rel error low']*100, 2), ") %")

        # print("\n campaign, ", camp)
        # print("\n Yield MxAOD")
        # print(round(cutflowDictMxAOD['no_selection']['yield'], 2))
        # print(round(cutflowDictMxAOD['trigger']['yield'], 2))
        # print(round(cutflowDictMxAOD['photon_2tightIDIS']['yield'], 2))
        # print(round(cutflowDictMxAOD['rel_pt']['yield'], 2))
        # print(round(cutflowDictMxAOD['diphoton_mass']['yield'], 2))
        # print(round(cutflowDictMxAOD['0lep']['yield'], 2))
        # print(round(cutflowDictMxAOD['AtLeast_2jets']['yield'], 2))
        # print(round(cutflowDictMxAOD['LessThan_6jets']['yield'], 2))
        # print(round(cutflowDictMxAOD['Exactly_2bjets']['yield'], 2))

        # print("\nYield Easyjet")
        # print(round(cutflowDictEasyjet['no_selection']['yield'], 2))
        # print(round(cutflowDictEasyjet['trigger']['yield'], 2))
        # print(round(cutflowDictEasyjet['photon_2tightIDIS']['yield'], 2))
        # print(round(cutflowDictEasyjet['rel_pt']['yield'], 2))
        # print(round(cutflowDictEasyjet['diphoton_mass']['yield'], 2))
        # print(round(cutflowDictEasyjet['0lep']['yield'], 2))
        # print(round(cutflowDictEasyjet['AtLeast_2jets']['yield'], 2))
        # print(round(cutflowDictEasyjet['LessThan_6jets']['yield'], 2))
        # print(round(cutflowDictEasyjet['Exactly_2bjets']['yield'], 2))

        # print("Eff MxAOD")
        # print(round(cutflowDictMxAOD['trigger']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['trigger']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['trigger']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['photon_2tightIDIS']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['rel_pt']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['rel_pt']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['rel_pt']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['diphoton_mass']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['diphoton_mass']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['diphoton_mass']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['0lep']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['0lep']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['0lep']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['AtLeast_2jets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['AtLeast_2jets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['AtLeast_2jets']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['LessThan_6jets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['LessThan_6jets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['LessThan_6jets']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['Exactly_2bjets']['cutflow eff']*100, 2), "(+", round(cutflowDictMxAOD['Exactly_2bjets']['cutflow error up']*100, 2), ", -", round(cutflowDictMxAOD['Exactly_2bjets']['cutflow error low']*100, 2), ") %")

        # print("Eff Easyjet")
        # print(round(cutflowDictEasyjet['trigger']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['trigger']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['trigger']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['photon_2tightIDIS']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['rel_pt']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['rel_pt']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['rel_pt']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['diphoton_mass']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['diphoton_mass']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['diphoton_mass']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['0lep']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['0lep']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['0lep']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['AtLeast_2jets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['AtLeast_2jets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['AtLeast_2jets']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['LessThan_6jets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['LessThan_6jets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['LessThan_6jets']['cutflow error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['Exactly_2bjets']['cutflow eff']*100, 2), "(+", round(cutflowDictEasyjet['Exactly_2bjets']['cutflow error up']*100, 2), ", -", round(cutflowDictEasyjet['Exactly_2bjets']['cutflow error low']*100, 2), ") %")

        # print("REL Eff MxAOD")
        # print(round(cutflowDictMxAOD['trigger']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['trigger']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['trigger']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['photon_2tightIDIS']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['photon_2tightIDIS']['rel error up']*100, 2), " , -", round(cutflowDictMxAOD['photon_2tightIDIS']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['rel_pt']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['rel_pt']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['rel_pt']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['diphoton_mass']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['diphoton_mass']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['diphoton_mass']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['0lep']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['0lep']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['0lep']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['AtLeast_2jets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['AtLeast_2jets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['AtLeast_2jets']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['LessThan_6jets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['LessThan_6jets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['LessThan_6jets']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictMxAOD['Exactly_2bjets']['rel eff']*100, 2), "(+", round(cutflowDictMxAOD['Exactly_2bjets']['rel error up']*100, 2), ", -", round(cutflowDictMxAOD['Exactly_2bjets']['rel error low']*100, 2), ") %")

        # print("REL Eff Easyjet")
        # print(round(cutflowDictEasyjet['trigger']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['trigger']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['trigger']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['photon_2tightIDIS']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['photon_2tightIDIS']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['photon_2tightIDIS']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['rel_pt']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['rel_pt']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['rel_pt']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['diphoton_mass']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['diphoton_mass']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['diphoton_mass']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['0lep']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['0lep']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['0lep']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['AtLeast_2jets']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['AtLeast_2jets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['AtLeast_2jets']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['LessThan_6jets']['rel eff']*100, 2), "(+", round(cutflowDictEasyjet['LessThan_6jets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['LessThan_6jets']['rel error low']*100, 2), ") %")
        # print(round(cutflowDictEasyjet['Exactly_2bjets']['rel eff']*100, 2), " (+", round(cutflowDictEasyjet['Exactly_2bjets']['rel error up']*100, 2), ", -", round(cutflowDictEasyjet['Exactly_2bjets']['rel error low']*100, 2), ") %")