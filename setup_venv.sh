export ENV="venv"
export ENV_DIR=$PWD/$ENV
source $ENV_DIR/bin/activate

export PYTHONPATH=$ENV_DIR/lib/python3.9/site-packages/:$PWD/:$PYTHONPATH
#export PATH=$PWD/scripts/:$PATH

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
setupATLAS
lsetup "views LCG_104c_ATLAS_5 /x86_64-el9-gcc13-opt"