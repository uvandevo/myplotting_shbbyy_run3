import ROOT

## Dictionaries with different variables:
# SampleMap() - map between sample and file path
# CampaignMap() - map between MC campaign and r- and p-tag
# DataCampaignMap() - map between data campaign and file name
# SampleValidationDict() - samples for the kinematic variables plots
# SampleDataMCDict() - samples for the Data/MC validation
# KinematicVarPlotDict() - plotting details for the kinematic variables plot
# VariablesDict() - Variable names in the easyjet ntuples and mxaod to define the rdf columns
# SelectionDict() - Definition of all selections for the cutflow in the easyjet ntuples and mxaod to use as filter for the rdfs
# SelectionDictSkimmedSamples() - Same as above, but for skimmed easyjet ntuples

# Samples map to files in /eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/
def SampleMap():
    dict = {
    'mX_750_mS_400': {
        'did': '801370',
        'file_name_easyjet': 'XHS_X750_S400_HyySbb',
        'file_name_mxaod': 'Py8_XHS_X750_S400_HyySbb_AF2.MxAODDetailedNoSkim.e8312_a875',
        'p_tag_mxaod': 'p4615'
    },

    'mX_750_mS_170': {
        'did': '801367',
        'file_name_easyjet': 'XHS_X750_S170_HyySbb',
        'file_name_mxaod': 'Py8_XHS_X750_S170_HyySbb_AF2.MxAODDetailedNoSkim.e8312_a875',
        'p_tag_mxaod': 'p4615'
    },

    'mX_245_mS_090': {
        'did': '800913',
        'file_name_easyjet': 'XHS_X245_S090_HyySbb',
        'file_name_mxaod': 'Py8_XHS_X245_S090_HyySbb_AF2.MxAODDetailedNoSkim.e8312_a875',
        'p_tag_mxaod': 'p4615'
    },

    'mX_205_mS_030': {
        'did': '800915',
        'file_name_easyjet': 'XHS_X205_S030_HyySbb',
        'file_name_mxaod': 'Py8_XHS_X205_S030_HyySbb_AF2.MxAODDetailedNoSkim.e8312_a875',
        'p_tag_mxaod': 'p4615'
    },

    'mX_170_mS_030': {
        'did': '800904',
        'file_name_easyjet': 'XHS_X170_S030_HyySbb',
        'file_name_mxaod': 'Py8_XHS_X170_S030_HyySbb_AF2.MxAODDetailedNoSkim.e8312_a875',
        'p_tag_mxaod': 'p4615'
    },
    
    'data15': {
        'folder_path_easyjet': 'SH_data_v2.data15_13TeV.periodAllYear'
        },

    'data16': {
        'folder_path_easyjet': 'SH_data_v2.data16_13TeV.periodAllYear'
        },

    'data17': {
        'folder_path_easyjet': 'SH_data_v2.data17_13TeV.periodAllYear'
        },

    'data18': {
        'folder_path_easyjet': 'SH_data_v2.data18_13TeV.periodAllYear'
        },

    'ggFHHbbyy': {
        'folder_path_easyjet': 'SH_bkg_v2.600021.e8222_s3681',
        'file_nmr_a': '38157187._000001',
        'file_nmr_d': '38157200._000001',
        'file_nmr_e': '38157215._000001',
        'file_name_mxaod': 'PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.e8222_s3126',
        'p_tag_mxaod': 'p4239'
        },

    'VBFHHbbyy': {
        'folder_path_easyjet': 'SH_bkg_v2.503004.e8263_s3681',
        'file_nmr_a': '38157403._000001',
        'file_nmr_d': '38157419._000001',
        'file_nmr_e': '38157436._000002',
        'file_name_mxaod': 'MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126',
        'p_tag_mxaod': 'p4239'
        },

    'ggFHyy': {
        'folder_path_easyjet': 'SH_bkg_v2.343981.e5607_s3681',
        'file_nmr_a': '38157634._000001',
        'file_nmr_d': '38157672._000002',
        'file_nmr_e': '38157694._000001',
        'file_name_mxaod': 'PowhegPy8_NNLOPS_ggH125.MxAODDetailedNoSkim.e5607_s3126',
        'p_tag_mxaod': 'p4180'
        },

    'VBFHyy': {
        'folder_path_easyjet': 'SH_bkg_v2.346214.e6970_s3681',
        'file_nmr_a': '38157926._000002',
        'file_nmr_d': '38157957._000001',
        'file_nmr_e': '38157986._000001',
        'file_name_mxaod': 'PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailedNoSkim.e6970_s3126',
        'p_tag_mxaod': 'p4180'
        },

    'W+Hyy': {
        'folder_path_easyjet': 'SH_bkg_v2.345318.e5734_s3681',
        'file_nmr_a': '38158194._000001',
        'file_nmr_d': '38158205._000001',
        'file_nmr_e': '38158216._000001',
        'file_name_mxaod': 'PowhegPy8_WpH125J.MxAODDetailedNoSkim.e5734_s3126',
        'p_tag_mxaod': 'p4207'
        },

    'W-Hyy': {
        'folder_path_easyjet': 'SH_bkg_v1.345317.e5734_s3681',
        'file_nmr_a': '38158395._000001',
        'file_nmr_d': '38158407._000001',
        'file_nmr_e': '38158416._000001',
        'file_name_mxaod': 'PowhegPy8_WmH125J.MxAODDetailedNoSkim.e5734_s3126',
        'p_tag_mxaod': 'p4207'
        },

    'qqZHyy': {
        'folder_path_easyjet': 'SH_bkg_v2.345319.e5743_s3681',
        'file_nmr_a': '38158581._000001',
        'file_nmr_d': '38158595._000001',
        'file_nmr_e': '38158614._000001',
        'file_name_mxaod': 'PowhegPy8_ZH125J.MxAODDetailedNoSkim.e5743_s3126',
        'p_tag_mxaod': 'p4207'
        },

    'ggZHyy': {
        'folder_path_easyjet': 'SH_bkg_v2.345061.e5762_s3681',
        'file_nmr_a': '38158797._000001',
        'file_nmr_d': '38158823._000001',
        'file_nmr_e': '38158844._000001',
        'file_name_mxaod': 'PowhegPy8_ggZH125.MxAODDetailedNoSkim.e5762_s3126',
        'p_tag_mxaod': 'p4207'
        },

    'ttHyy': {
        'folder_path_easyjet': 'SH_bkg_v2.346525.e7488_s3681',
        'file_nmr_a': '38159021._000001',
        'file_nmr_d': '38159034._000001',
        'file_nmr_e': '38159051._000001',
        'file_name_mxaod': 'PowhegPy8_ttH125_fixweight.MxAODDetailedNoSkim.e7488_s3126',
        'p_tag_mxaod': 'p4180'
        },

    'yy+jets': {
        'folder_path_easyjet': 'SH_bkg_v2.364352.e6452_s3681',
        'file_nmr_a': '38159182._000001',
        'file_nmr_d': '38159196._000001',
        'file_nmr_e': '38159209._000001',
        'file_name_mxaod': 'Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875',
        'p_tag_mxaod': 'p4204'
        },

    'ttyy_noallhad': {
        'folder_path_easyjet': 'SH_bkg_v2.508781.e8448_s3681',
        'file_nmr_a': '38159754._000001',
        'file_nmr_d': '38159766._000002',
        'file_nmr_e': '38159782._000001',
        'file_name_mxaod': 'MGPy8_ttgammagamma_noallhad_AF2.MxAODDetailed.e6542_a875',
        'p_tag_mxaod': 'p4204'
        }
    }
    
    return dict

# Campaign map
def CampaignMap():
    dict = {
    "sig mc20a": "r14859_p6026",
    "sig mc20d": "r14860_p6026",
    "sig mc20e": "r14861_p6026",
    "bkg mc20a": "r13167_p5855",
    "bkg mc20d": "r13144_p5855",
    "bkg mc20e": "r13145_p5855",
    "mc16a": "r9364",
    "mc16d": "r10201",
    "mc16e": "r10724"
    }
    return dict

# Data campaign map for Easyjet ntuples
def DataCampaignMap():
    dict = {
    "15": "grp15_v01_p5855",
    "16": "grp16_v01_p5855",
    "17": "grp17_v01_p5855",
    "18": "grp18_v01_p5855"
    }
    return dict

def SampleValidationDict():
    dict = {
        ## Signals
        'mX_750_mS_400': {
            'histoname': 'mX_750_mS_400',
            'extra label': 'm_{X}=750 GeV, m_{S}=400 GeV'
        },

        'mX_750_mS_170': {
            'histoname': 'mX_750_mS_170',
            'extra label': 'm_{X}=750 GeV, m_{S}=170 GeV'
        },

        'mX_245_mS_090': {
            'histoname': 'mX_245_mS_090',
            'extra label': 'm_{X}=245 GeV, m_{S}=90 GeV'
        },

        'mX_205_mS_030': {
            'histoname': 'mX_205_mS_030',
            'extra label': 'm_{X}=205 GeV, m_{S}=30 GeV'
        },

        'mX_170_mS_030': {
            'histoname': 'mX_170_mS_030',
            'extra label': 'm_{X}=170 GeV, m_{S}=30 GeV'
        },
        ## Backgrounds
        'ggFHHbbyy': {
            'histoname': 'ggF_HH',
            'extra label': 'ggF HH #rightarrow b#bar{b}#gamma#gamma'
        },

        'VBFHHbbyy': {
            'histoname': 'VBF_HH',
            'extra label': 'VBF HH #rightarrow b#bar{b}#gamma#gamma'
        },

        'ggFHyy': {
            'histoname': 'ggF_Hyy',
            'extra label': 'ggF H #rightarrow #gamma#gamma'
        },

        'VBFHyy': {
            'histoname': 'VBF_Hyy',
            'extra label': 'VBF H #rightarrow #gamma#gamma'
        },

        'W+Hyy': {
            'histoname': 'WpHyy',
            'extra label': 'W^{+}H(#rightarrow #gamma#gamma)'
        },

        'W-Hyy': {
            'histoname': 'WmHyy',
            'extra label': 'W^{+}H(#rightarrow #gamma#gamma)'
        },

        'qqZHyy': {
            'histoname': 'qqZHyy',
            'extra label': 'qq #rightarrow ZH(#rightarrow #gamma#gamma)'
        },

        'ggZHyy': {
            'histoname': 'ggZHyy',
            'extra label': 'gg #rightarrow ZH(#rightarrow #gamma#gamma)'
        },

        'ttHyy': {
            'histoname': 'ttHyy',
            'extra label': 'ttH(#rightarrow #gamma#gamma)'
        },

        'yy+jets': {
            'histoname': 'yy_jets',
            'extra label': '#gamma#gamma+jets',
        },

        'ttyy_allhad': {
            'histoname': 'ttyy_allhad',
            'extra label': 't#bar{t}#gamma#gamma (allhad)',
        },

        'ttyy_noallhad': {
            'histoname': 'ttyy_noallhad',
            'extra label': 't#bar{t}#gamma#gamma (noallhad)',
        },

        'Zqq_yy': {
            'histoname': 'Zqq_yy',
            'extra label': 'Z (#rightarrow q#bar{q}) #gamma#gamma',
        },

    }
    
    return dict

def SampleDataMCDict():
    dict = {
        'HH': {
            'histoname': 'HH',
            'color': (222, 90, 106),
            'legend description': 'HH'
        },

        'Hyy': {
            'histoname': 'Hyy',
            'color': (248, 206, 104),
            'legend description': 'Single Higgs'
        },

        'yy_jets': {
            'histoname': 'yy_jets',
            'color': (100, 192, 232),
            'legend description': '#gamma#gamma+jets',
        },

        'ttyy': {
            'histoname': 'ttyy',
            'color': (155, 152, 204),
            'legend description': 't#bar{t}#gamma#gamma',
        },

        'Zqq_yy': {
            'histoname': 'Zqq_yy',
            'color': (250, 202, 255),
            'legend description': 'Z (#rightarrow qq) #gamma#gamma',
        },

        'data': {
            'histoname': 'data',
            'legend description': 'Data',
        }
    }
    
    return dict

def KinematicVarPlotDict():
    dict = {
        'Leading_photon_pt': { 
            'variable': 'y1_pt',
            'x-axis title': 'Leading photon p_{T} [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 400,
            'nBins' : 40
        },

        'Subleading_photon_pt': {
            'variable': 'y2_pt',
            'x-axis title': 'Subleading photon p_{T} [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 200,
            'nBins' : 20
        },

        'Leading_photon_e': {
            'variable': 'y1_e',
            'x-axis title': 'Leading photon E [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 800,
            'nBins' : 40
        },

        'Subleading_photon_e': {
            'variable': 'y2_e',
            'x-axis title': 'Subleading photon E [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 500,
            'nBins' : 25
        },

        'Leading_photon_eta': {
            'variable': 'y1_eta',
            'x-axis title': 'Leading photon #eta',
            'units' : '',
            'xMin' : -3,
            'xMax' : 3,
            'nBins' : 24
        },

        'Subleading_photon_eta': {
            'variable': 'y2_eta',
            'x-axis title': 'Subleading photon #eta',
            'units' : '',
            'xMin' : -3,
            'xMax' : 3,
            'nBins' : 24
        },

        'Leading_photon_phi': {
            'variable': 'y1_phi',
            'x-axis title': 'Leading photon #phi [rad]',
            'units' : 'rad',
            'xMin' : -5,
            'xMax' : 5,
            'nBins' : 20
        },

        'Subleading_photon_phi': {
            'variable': 'y2_phi',
            'x-axis title': 'Subleading photon #phi [rad]',
            'units' : 'rad',
            'xMin' : -5,
            'xMax' : 5,
            'nBins' : 20
        },

        'bjet1_pt': {
            'variable': 'b1_pt',
            'x-axis title': 'b-jet 1 p_{T} [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 500,
            'nBins' : 25
        },

        'bjet2_pt': {
            'variable': 'b2_pt',
            'x-axis title': 'b-jet 2 p_{T} [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 500,
            'nBins' : 25
        },

        'bjet1_eta': {
            'variable': 'b1_eta',
            'x-axis title': 'b-jet 1 #eta',
            'units' : '',
            'xMin' : -3,
            'xMax' : 3,
            'nBins' : 24
        },

        'bjet2_eta': {
            'variable': 'b2_eta',
            'x-axis title': 'b-jet 2 #eta',
            'units' : '',
            'xMin' : -3,
            'xMax' : 3,
            'nBins' : 24
        },

        'bjet1_phi': {
            'variable': 'b1_phi',
            'x-axis title': 'b-jet 1 #phi [rad]',
            'units' : 'rad',
            'xMin' : -5,
            'xMax' : 5,
            'nBins' : 20
        },

        'bjet2_phi': {
            'variable': 'b2_phi',
            'x-axis title': 'b-jet 2 #phi [rad]',
            'units' : 'rad',
            'xMin' : -5,
            'xMax' : 5,
            'nBins' : 20
        },

        'dPhi_yy': {
            'variable': 'dPhi_yy',
            'x-axis title': '#Delta#phi_{#gamma#gamma}',
            'units' : '',
            'xMin' : -0.5,
            'xMax' : 4.5,
            'nBins' : 20
        },

        'dR_yy': {
            'variable': 'dR_yy',
            'x-axis title': '#DeltaR_{#gamma#gamma}',
            'units' : '',
            'xMin' : -0.5,
            'xMax' : 4.5,
            'nBins' : 20
        },

        'dPhi_bb': {
            'variable': 'dPhi_bb',
            'x-axis title': '#Delta#phi_{bb}',
            'units' : '',
            'xMin' : -0.5,
            'xMax' : 4.5,
            'nBins' : 20
        },

        'myy_inv_mass': {
            'variable': 'm_yy',
            'x-axis title': 'm_{#gamma#gamma} [GeV]',
            'units' : 'GeV',
            'xMin' : 105,
            'xMax' : 160,
            'nBins' : 55
        },

        'myy_hardestVertex_inv_mass': {
            'variable': 'm_yy_hardestVertex',
            'x-axis title': 'm_{#gamma#gamma} [GeV]',
            'units' : 'GeV',
            'xMin' : 115,
            'xMax' : 135,
            'nBins' : 40
        },

        'myy_RatioPVs_inv_mass': {
            'variable': 'm_yy_ratioPVs',
            'x-axis title': 'NN Vertex/Hardest Vertex',
            'units' : '',
            'xMin' : 0.99,
            'xMax' : 1.01,
            'nBins' : 20
        },

        # Change the x-range for mbb and mbbyy s.t it's dependent on the mX and mS?
        'mbb_inv_mass': {
            'variable': 'm_bb',
            'x-axis title': 'm_{bb} [GeV]',
            'units' : 'GeV',
            'xMin' : 0,
            'xMax' : 300,
            'nBins' : 60
        },

        'mbbyy_inv_mass': {
            'variable': 'm_bbyy',
            'x-axis title': 'm_{bb#gamma#gamma} [GeV]',
            'units' : 'GeV',
            'xMin' : 200,
            'xMax' : 1000,
            'nBins' : 80
        },

    }

    return dict

def VariablesDict():
    dict = {
        'y1_pt': { 
            'easyjet': 'bbyy_Photon1_pt_NOSYS/1000',    #divided by 1000 to get unit GeV
            'mxaod': 'HGamEventInfoAuxDyn.pT_y1/1000'
        },

        'y2_pt': { 
            'easyjet': 'bbyy_Photon2_pt_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.pT_y2/1000'
        },

        'y1_e': { 
            'easyjet': 'bbyy_Photon1_E_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.E_y1/1000'
        },

        'y2_e': { 
            'easyjet': 'bbyy_Photon2_E_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.E_y2/1000'
        },

        'y1_eta': {
            'easyjet': 'bbyy_Photon1_eta_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.eta_y1'
        },

        'y2_eta': {
            'easyjet': 'bbyy_Photon2_eta_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.eta_y2'
        },

        'y1_phi': {
            'easyjet': 'bbyy_Photon1_phi_NOSYS',
            'mxaod': 'HGamPhotonsAuxDyn.phi[0]'
        },

        'y2_phi': {
            'easyjet': 'bbyy_Photon2_phi_NOSYS',
            'mxaod': 'HGamPhotonsAuxDyn.phi[1]'
        },

        'b1_pt': {
            'easyjet': 'bbyy_HbbCandidate_Jet1_pt_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_pT_jet1/1000'
        },

        'b2_pt': {
            'easyjet': 'bbyy_HbbCandidate_Jet2_pt_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_pT_jet2/1000'
        },

        'b1_eta': {
            'easyjet': 'bbyy_HbbCandidate_Jet1_eta_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_eta_jet1'
        },

        'b2_eta': {
            'easyjet': 'bbyy_HbbCandidate_Jet2_eta_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_eta_jet2'
        },

        'b1_phi': {
            'easyjet': 'bbyy_HbbCandidate_Jet1_phi_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_phi_jet1'
        },

        'b2_phi': {
            'easyjet': 'bbyy_HbbCandidate_Jet2_phi_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_phi_jet2'
        },

        'dPhi_yy': {
            'easyjet': 'dphi(y1_phi, y2_phi)',
            'mxaod': 'HGamEventInfoAuxDyn.Dphi_y_y'
        },

        'dR_yy': {
            'easyjet': 'bbyy_dRyy_NOSYS',
            'mxaod': 'HGamEventInfoAuxDyn.DR_y_y'
        },

        'dPhi_bb': {
            'easyjet': 'dphi(b1_phi, b2_phi)',
            'mxaod': 'dphi(b1_phi, b2_phi)'
        },

        # 'dR_bb': {
        #     'easyjet': 'bbyy_dRbb_NOSYS',
        #     'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_deltaR_jj'
        # },

        'm_yy': {
            'easyjet': 'bbyy_myy_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.m_yy/1000'
        },

        'm_yy_hardestVertex': {
            'easyjet': 'bbyy_myy_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.m_yy_hardestVertex/1000'
        },

        'm_yy_ratioPVs': {
            'easyjet': '1',
            'mxaod': 'HGamEventInfoAuxDyn.m_yy/HGamEventInfoAuxDyn.m_yy_hardestVertex'
        },

        'm_bb': {
            'easyjet': 'bbyy_mbb_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_m_jj/1000'
        },

        'm_bbyy': {
            'easyjet': 'bbyy_mbbyy_NOSYS/1000',
            'mxaod': 'HGamEventInfoAuxDyn.yybb_BCal_m_yyjj/1000'
        },

        'noSel': {
            'easyjet': '1',
            'mxaod': '1'
        },

    }

    return dict

# OBS reformulate the selections for Easyjet to use the cutflow histogram instead if the ntuple was made with bypass = true
def SelectionDict():
    dict = {
        'no_selection': {
            'legend upper': 'Before selection',
            'easyjet': 'noSel > 0',
            'mxaod': 'noSel > 0'
        },

        'trigger': {
            'legend upper': 'Pass trigger',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0',
            'mxaod': '(EventInfoAuxDyn.passTrig_HLT_g35_loose_g25_loose) || (EventInfoAuxDyn.passTrig_HLT_g35_medium_g25_medium_L12EM20VH) || (EventInfoAuxDyn.passTrig_HLT_g120_loose) || (EventInfoAuxDyn.passTrig_HLT_g140_loose)'
        },

        'photon_2tightIDIS': {
            'legend upper': '2 tight id+iso photons',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.cutFlow > 12'
        },

        'rel_pt': {
            'legend upper': 'Pass rel pt',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.cutFlow > 13'
        },

        'diphoton_mass': {
            'legend upper': 'Preselection (diphoton mass window)',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0 && bbyy_DIPHOTON_MASS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.cutFlow > 14 || HGamEventInfoAuxDyn.isPassed == 1'
        },

        '0lep': {
            'legend upper': 'Zero leptons',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0 && bbyy_DIPHOTON_MASS_NOSYS > 0 && bbyy_EXACTLY_ZERO_LEPTONS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.N_lep == 0'
        },

        'AtLeast_2jets': {
            'legend upper': 'At least 2 jets',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0 && bbyy_DIPHOTON_MASS_NOSYS > 0 && bbyy_EXACTLY_ZERO_LEPTONS_NOSYS > 0 && bbyy_AT_LEAST_TWO_JETS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.N_lep == 0 && HGamEventInfoAuxDyn.N_j >= 2'
        },

        'LessThan_6jets': {
            'legend upper': 'Less than 6 jets',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0 && bbyy_DIPHOTON_MASS_NOSYS > 0 && bbyy_EXACTLY_ZERO_LEPTONS_NOSYS > 0 && bbyy_AT_LEAST_TWO_JETS_NOSYS > 0 && bbyy_LESS_THAN_SIX_CENTRAL_JETS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.N_lep == 0 && HGamEventInfoAuxDyn.N_j >= 2 && HGamEventInfoAuxDyn.N_j_central < 6'
        },

        'Exactly_2bjets': {
            'legend upper': 'Exactly 2 b-jets',
            'easyjet': 'bbyy_PASS_TRIGGER_SINGLE_OR_DIPHOTON_NOSYS > 0 && bbyy_TWO_TIGHTID_ISO_PHOTONS_NOSYS > 0 && bbyy_PASS_RELPT_NOSYS > 0 && bbyy_DIPHOTON_MASS_NOSYS > 0 && bbyy_EXACTLY_ZERO_LEPTONS_NOSYS > 0 && bbyy_AT_LEAST_TWO_JETS_NOSYS > 0 && bbyy_LESS_THAN_SIX_CENTRAL_JETS_NOSYS > 0 && bbyy_EXACTLY_TWO_B_JETS_NOSYS > 0',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.yybb_btag77_cutFlow == 7'
        }
    }

    return dict

def SelectionDictSkimmedSamples():
    dict = {
        # 'no_selection': {
        #     'legend upper': 'Before selection',
        #     'easyjet': 'noSel > 0',
        #     'mxaod': 'noSel > 0'
        # },

        # 'trigger': {
        #     'legend upper': 'Pass trigger double',
        #     'easyjet': 'pass_trigger_diphoton_NOSYS > 0',
        #     'mxaod': '(EventInfoAuxDyn.passTrig_HLT_g35_loose_g25_loose) || (EventInfoAuxDyn.passTrig_HLT_g35_medium_g25_medium_L12EM20VH) || (EventInfoAuxDyn.passTrig_HLT_g120_loose) || (EventInfoAuxDyn.passTrig_HLT_g140_loose)'
        # },

        'AtLeast_2jets': {
            'legend upper': 'At least 2 jets',
            'easyjet': 'bbyy_nJets_NOSYS >= 2',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.N_lep == 0 && HGamEventInfoAuxDyn.N_j >= 2'
        },


        'LessThan_6jets': {
            'legend upper': 'Less than 6 jets',
            'easyjet': 'bbyy_nCentralJets_NOSYS < 6',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.N_lep == 0 && HGamEventInfoAuxDyn.N_j >= 2 && HGamEventInfoAuxDyn.N_j_central < 6'
        },

        'Exactly_2bjets': {
            'legend upper': 'Exactly 2 b-jets',
            'easyjet': 'bbyy_nCentralJets_NOSYS < 6 && bbyy_nBJets_NOSYS == 2',
            'mxaod': 'HGamEventInfoAuxDyn.isPassed == 1 && HGamEventInfoAuxDyn.yybb_btag77_cutFlow == 7'
        }
    }

    return dict