# myplotting_SHbbyy_Run3

## Context
My plotting and cutflow scripts for the validation study for Run2+partial Run3 SHbbbyy analysis.

## Setup
For first time setup run:
```
source install.sh
```
to create a venv to run the scripts. Then for every new login/terminal start, run:
```
source setup_venv.sh
```

## Structure
The scripts contains:
- `variablesDict.py`: different dictonaries with sample paths, kinematic variable definitions, histogram definitions, selections
- `processList.py`: different lists defining which variables to plot, which selections to apply and which samples and campaigns to include
- `myUtils.py`: functions being used in other scripts
- `plot.py`: main script for plotting
- `cutflow.py`: main script for producing cutflows
- `cutflow_allCamps.py`: similar scripts as above, but merging campaigns a, d and e
