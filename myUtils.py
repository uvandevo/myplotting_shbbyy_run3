import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = False
import os, glob
import sys
import math
import numpy as np
from variablesDicts import *

## Function to get the efficiency of a cut and the upper and lower error computed from Clopper-Pearson statistics with 68% confidence level
def getEfficiency(nTotal, nPass):
    efficiency = nPass/nTotal
    lower_error = efficiency - ROOT.TEfficiency.ClopperPearson(nTotal, nPass,  0.683, False)
    upper_error = ROOT.TEfficiency.ClopperPearson(nTotal, nPass,  0.683,  True) - efficiency

    return efficiency, lower_error, upper_error

## Function to book histograms
def histmaker(df, samples, title, xlabel, ylabel, nbins, xmin, xmax, variable):
    hists = {}
    for s in samples:
        hists[s] = df[s].Histo1D(ROOT.RDF.TH1DModel(s, "{}; {};{}".format(title, xlabel, ylabel), nbins, xmin, xmax),
                "{}".format(variable), "total_weight")
    return hists

## Function to get sum of weights from the mxaod files
def getYieldFactors_mxaod(f_MxAOD, campaign):
    # Get sum of weights for MxAOD from the histogram Cutflow <sample> _noDalitz_weighted
    histos_names = f_MxAOD.GetListOfKeys()
    hist_name = [k.GetName() for k in f_MxAOD.GetListOfKeys() if "_noDalitz_weighted" in k.GetName()]
    #print(len(hist_name))
    hist = f_MxAOD.Get(hist_name[0])

    if campaign == 'a': lumi = 36646.74 #pb-1
    if campaign == 'd': lumi = 44630.6  #pb-1
    if campaign == 'e': lumi = 58791.6  #pb-1

    nTotalGenEventsMxAOD = hist.GetBinContent(1)    #Number of generated events in MxAOD
    nFilteredGenEventsMxAOD = hist.GetBinContent(2)    #Number of generated events in DxAOD, some backgrounds may have skimmed events
    sum_of_weights_noDalitz_MxAOD = hist.GetBinContent(3)
    # Rescale sum of weights in case the sample has filtered events between MxAOD and DxAOD
    sum_of_weights_noDalitz_MxAOD = (nTotalGenEventsMxAOD/nFilteredGenEventsMxAOD)*sum_of_weights_noDalitz_MxAOD
    if 'HyySbb' in str(f_MxAOD): SFYieldMxAOD = lumi*1E-3/sum_of_weights_noDalitz_MxAOD #to get pb of the cross section
    else: SFYieldMxAOD = lumi/sum_of_weights_noDalitz_MxAOD
    
    return SFYieldMxAOD, sum_of_weights_noDalitz_MxAOD



## Function to get SFyield for the weights
## Old method
def getSFyields(f_Easyjet, f_MxAOD, f_weightEasyjet, campaign):
    # The Easyjet events needs rescaling, first get total number of generated events from CutBookkeeper <sample> _NOSYS
    histos_names = f_Easyjet.GetListOfKeys()
    hist_name = [k.GetName() for k in f_Easyjet.GetListOfKeys() if "CutBookkeeper_" in k.GetName() and "_NOSYS" in k.GetName()]
    histoTotalEvents = f_Easyjet.Get(hist_name[0])
    nTotalGenEventsEasyjet = histoTotalEvents.GetBinContent(1)
    # Get number of events passing event cleaning and other possible filters
    histoFilteredEvents = f_Easyjet.Get("EventsPassed_BinLabeling")
    nFilteredGenEventsEasyjet = histoFilteredEvents.GetBinContent(1)
    # Get scale factor
    scaleEasyjet = nTotalGenEventsEasyjet/nFilteredGenEventsEasyjet

    # Define a rdf from the post-process file
    df_weightEasyjet = ROOT.RDataFrame('AnalysisMiniTree', f_weightEasyjet)
    # Get sum of weights, some backgrounds need to filter out Dalitz events, hence the labeling
    # sum_of_weights_noDalitz_Easyjet = df_weightEasyjet.Mean('sumOfWeights').GetValue() * scaleEasyjet
    sum_of_weights_noDalitz_Easyjet = df_weightEasyjet.Mean('sumOfWeights').GetValue()

    SFEasyjet = 1/sum_of_weights_noDalitz_Easyjet
    if 'HyySbb' in str(f_Easyjet): xsection = 1E-3
    else: xsection = df_weightEasyjet.Mean('AMIXsection').GetValue()
    SFYieldEasyjet = xsection * df_weightEasyjet.Mean('kFactor').GetValue() * df_weightEasyjet.Mean('Luminosity').GetValue() * 1e3 * df_weightEasyjet.Mean('FilterEff').GetValue() / sum_of_weights_noDalitz_Easyjet

    # Get sum of weights for MxAOD from the histogram Cutflow <sample> _noDalitz_weighted
    histos_names = f_MxAOD.GetListOfKeys()
    hist_name = [k.GetName() for k in f_MxAOD.GetListOfKeys() if "_noDalitz_weighted" in k.GetName()]
    #print(len(hist_name))
    hist = f_MxAOD.Get(hist_name[0])

    if campaign == 'a': lumi = 36646.74 #pb-1
    if campaign == 'd': lumi = 44630.6  #pb-1
    if campaign == 'e': lumi = 58791.6  #pb-1

    nTotalGenEventsMxAOD = hist.GetBinContent(1)    #Number of generated events in MxAOD
    nFilteredGenEventsMxAOD = hist.GetBinContent(2)    #Number of generated events in DxAOD, some backgrounds may have skimmed events
    sum_of_weights_noDalitz_MxAOD = hist.GetBinContent(3)
    # Rescale sum of weights in case the sample has filtered events between MxAOD and DxAOD
    sum_of_weights_noDalitz_MxAOD = (nTotalGenEventsMxAOD/nFilteredGenEventsMxAOD)*sum_of_weights_noDalitz_MxAOD
    if 'HyySbb' in str(f_MxAOD): SFYieldMxAOD = lumi*1E-3/sum_of_weights_noDalitz_MxAOD #to get pb of the cross section
    else: SFYieldMxAOD = lumi/sum_of_weights_noDalitz_MxAOD
    
    SFMxAOD = 1/sum_of_weights_noDalitz_MxAOD

    return SFYieldEasyjet, SFYieldMxAOD, SFEasyjet, SFMxAOD, scaleEasyjet, nTotalGenEventsEasyjet

# ## Function to get SFyield for the weights
# def getSFyieldEasyjet(f_Easyjet, f_weightEasyjet):
#     # The Easyjet events needs rescaling, first get total number of generated events from CutBookkeeper <sample> _NOSYS
#     histos_names = f_Easyjet.GetListOfKeys()
#     hist_name = [k.GetName() for k in f_Easyjet.GetListOfKeys() if "CutBookkeeper_" in k.GetName() and "_NOSYS" in k.GetName()]
#     histoTotalEvents = f_Easyjet.Get(hist_name[0])
#     nTotalGenEventsEasyjet = histoTotalEvents.GetBinContent(1)
#     # Get number of events passing event cleaning and other possible filters
#     histoFilteredEvents = f_Easyjet.Get("EventsPassed_BinLabeling")
#     nFilteredGenEventsEasyjet = histoFilteredEvents.GetBinContent(1)
#     # Get scale factor
#     scaleEasyjet = nTotalGenEventsEasyjet/nFilteredGenEventsEasyjet

#     # Define a rdf from the post-process file
#     df_weightEasyjet = ROOT.RDataFrame('AnalysisMiniTree', f_weightEasyjet)
#     # Get sum of weights, some backgrounds need to filter out Dalitz events, hence the labeling
#     sum_of_weights_noDalitz_Easyjet = df_weightEasyjet.Mean('sumOfWeights').GetValue() * scaleEasyjet

#     SFEasyjet = 1/sum_of_weights_noDalitz_Easyjet
#     SFYieldEasyjet = df_weightEasyjet.Mean('AMIXsection').GetValue() * df_weightEasyjet.Mean('kFactor').GetValue() * df_weightEasyjet.Mean('Luminosity').GetValue() * df_weightEasyjet.Mean('FilterEff').GetValue() * 1e3 / sum_of_weights_noDalitz_Easyjet

#     return SFYieldEasyjet

def generatePostProcessFiles():
    # Get map of samples to files
    sampleMap = SampleMap()
    campaignMap = CampaignMap()
    #samples = ['mX_750_mS_170']
    #samples = ['mX_750_mS_400', 'mX_750_mS_170', 'mX_245_mS_090', 'mX_205_mS_030', 'mX_170_mS_030']
    samples = ['ggFHHbbyy', 'VBFHHbbyy', 'ggFHyy', 'VBFHyy', 'W+Hyy', 'W-Hyy', 'qqZHyy', 'ggZHyy', 'ttHyy', 'yy+jets', 'ttyy_noallhad']
    samples_dalitz = ['ggFHHbbyy', 'VBFHHbbyy', 'ggFHyy', 'VBFHyy', 'W+Hyy', 'W-Hyy', 'qqZHyy', 'ggZHyy', 'ttHyy']
    campaigns = ['a', 'd', 'e']

    scriptfile = open("producePostProcess_v3.sh","w")

    for sample in samples:
        for camp in campaigns:
            if 'mX' in sample:
                fileName = sampleMap[sample]['file_name_easyjet']
                did = sampleMap[sample]['did']
                tags = campaignMap['sig mc20{}'.format(camp)]
                path_in = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/Easyjet_{}_mc20{}_{}.root'.format(did,camp,fileName)
                path_out = "/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(fileName, tags)
            else:
                folderName = sampleMap[sample]['folder_path_easyjet']
                tags = campaignMap['bkg mc20{}'.format(camp)]
                fileName = sampleMap[sample]['file_nmr_{}'.format(camp)]
                path_in = '/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/user.uvandevo.{}_{}_TREE/user.uvandevo.{}.output-tree.root'.format(folderName,tags,fileName)
                path_out = "/eos/user/u/uvandevo/SHbbyyRun3/ntuples_v2/{}_{}_PostProcessed_ntuple.root".format(sample, tags)
            
            scriptfileline_infile = "bbyyPostProcess.py --inFile " + path_in + " \\\n"
            scriptfile.write(scriptfileline_infile)
            scriptfileline_xsection = "--xSectionsConfig bbyyAnalysis/XSectionData.yaml \\\n"
            scriptfile.write(scriptfileline_xsection)
            scriptfileline_outfile = "--outFile " + path_out + " \\\n"
            scriptfile.write(scriptfileline_outfile)
            scriptfileline_bTag = "--bTagWP DL1dv01_FixedCutBEff_77 \\\n"
            scriptfile.write(scriptfileline_bTag)
            if sample in samples_dalitz: scriptfileline_end = "--containDalitz 1\n\n"
            else: scriptfileline_end = "\n"
            scriptfile.write(scriptfileline_end)

def plotDiphotonInvMassComparison(hists, n_bins1, xlabel1, xmin1, xmax1, n_bins2, xlabel2, xmin2, xmax2, extraLabelProcess, extraLabelCampaign, selectionLabel, path_plots, fig_title, fig_format):
    # Run the event loop
    hardestVertex = hists["myy_hardestVertex_inv_mass"]
    NNVertex = hists["myy_inv_mass"]

    hardestVertex_n_events = hardestVertex.Integral()
    NNVertex_n_events = NNVertex.Integral()

    # Scale histograms to fraction of events
    hardestVertex.Scale(1/hardestVertex_n_events)
    NNVertex.Scale(1/NNVertex_n_events)

    # Set styles
    ROOT.gROOT.SetStyle("ATLAS")

    # Create canvas
    c = ROOT.TCanvas("c", "", 700, 750)

    upper_pad = ROOT.TPad("upper_pad", "", 0, 0.3, 1, 1)
    lower_pad = ROOT.TPad("lower_pad", "", 0, 0, 1, 0.3)
    for p in [upper_pad, lower_pad]:
        p.SetLeftMargin(0.14)
        p.SetRightMargin(0.05)
        p.SetTickx(True)
        p.SetTicky(True)
    upper_pad.SetBottomMargin(0.02)
    lower_pad.SetTopMargin(0)
    lower_pad.SetBottomMargin(0.3)

    upper_pad.Draw()
    lower_pad.Draw()

    # Draw histograms
    upper_pad.cd()

    hardestVertex.SetLineColor(ROOT.kBlue+1)
    maxHisto = hardestVertex.GetMaximum()
    heightY = 1.75*maxHisto/ROOT.gPad.GetUymax()
    lastbin = hardestVertex.GetNbinsX()
    overflowBin = lastbin + 1
    hardestVertex.SetBinContent(lastbin, hardestVertex.GetBinContent(lastbin) + hardestVertex.GetBinContent(overflowBin))
    # underflowBin = -1
    # hardestVertex.SetBinContent(0, hardestVertex.GetBinContent(0) + hardestVertex.GetBinContent(underflowBin))
    hardestVertex.SetMinimum(0)
    hardestVertex.SetMaximum(heightY)
    hardestVertex.SetLineStyle(1)
    hardestVertex.SetLineWidth(2)
    hardestVertex.GetXaxis().SetLabelOffset(999)
    hardestVertex.SetMarkerStyle(1)
    hardestVertex.Draw("E1 HIST")

    #print("hardestVertex histo mean: ", hardestVertex.GetMean())

    NNVertex.SetLineColor(ROOT.kOrange-3)
    lastbin = NNVertex.GetNbinsX()
    overflowBin = lastbin + 1
    NNVertex.SetBinContent(lastbin, NNVertex.GetBinContent(lastbin) + NNVertex.GetBinContent(overflowBin))
    # underflowBin = -1
    # NNVertex.SetBinContent(0, NNVertex.GetBinContent(0) + NNVertex.GetBinContent(underflowBin))
    NNVertex.SetLineStyle(1)
    NNVertex.SetLineWidth(2)
    NNVertex.GetXaxis().SetLabelOffset(999)
    NNVertex.SetMarkerStyle(1)
    NNVertex.Draw("E1 AH SAME")
    
    #print("NNVertex histo mean: ", NNVertex.GetMean())

    # Draw ratio
    lower_pad.cd()

    ratio_ref = ROOT.TF1("one", "1", xmin1, xmax1)
    ratio_ref.SetLineColor(ROOT.kBlack)
    ratio_ref.SetLineStyle(2)
    ratio_ref.SetLineWidth(2)
    ratio_ref.SetMinimum(0)
    ratio_ref.SetMaximum(2)
    ratio_ref.GetXaxis().SetLabelSize(0.08)
    ratio_ref.GetXaxis().SetTitleSize(0.12)
    ratio_ref.GetXaxis().SetTitleOffset(1.0)
    ratio_ref.GetYaxis().SetLabelSize(0.08)
    ratio_ref.GetYaxis().SetTitleSize(0.09)
    ratio_ref.GetYaxis().SetTitle("#frac{Hardest Vertex}{NN Vertex}")
    ratio_ref.GetYaxis().CenterTitle()
    ratio_ref.GetYaxis().SetTitleOffset(0.7)
    #ratio_ref.GetYaxis().SetNdivisions(503, False)
    ratio_ref.GetYaxis().ChangeLabel(-1, -1, 0)
    ratio_ref.GetXaxis().SetTitle("{}".format(xlabel1))
    ratio_ref.Draw()

    ratio = ROOT.TGraphErrors(n_bins1)
    for bin in range(n_bins1):
        hardestVertex_bin = hardestVertex.GetBinContent(bin)
        NNVertex_bin = NNVertex.GetBinContent(bin)
        x_value = hardestVertex.GetXaxis().GetBinCenter(bin)
        if hardestVertex_bin == 0.0:
            ratio.SetPoint(bin, x_value, 0)
            ratio.SetPointError(bin, 0, 0)
            continue
        if NNVertex_bin == 0.0: 
            ratio.SetPoint(bin, x_value, -99)
            ratio.SetPointError(bin, 0, 0)
            continue
        ratio_value = hardestVertex_bin/NNVertex_bin
        hardestVertex_error = hardestVertex.GetBinError(bin)
        hardestVertex_rel_error = hardestVertex_error/hardestVertex_bin
        NNVertex_error = NNVertex.GetBinError(bin)
        NNVertex_rel_error = NNVertex_error/NNVertex_bin
        ratio_error = ratio_value * math.sqrt(hardestVertex_rel_error**2 + NNVertex_rel_error**2)
        ratio.SetPoint(bin, x_value, ratio_value)
        ratio.SetPointError(bin, 0, ratio_error)

    ratio.SetMarkerStyle(20)
    ratio.SetMarkerSize(1)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetLineWidth(2)
    ratio.Draw("SAME P")

    diff_mean = abs(NNVertex.GetMean() - hardestVertex.GetMean())*1E3

    # Add legend with mean values
    upper_pad.cd()
    legend = ROOT.TLegend(0.53, 0.65, 0.9, 0.9)
    legend.SetTextFont(42)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.05)
    legend.SetTextAlign(12)
    legend.AddEntry(NNVertex, "NN Vertex:" ,"le")
    legend.SetTextSize(0.04)
    legend.AddEntry("", "mean = {} GeV".format(round(NNVertex.GetMean(), 2)), "")
    legend.AddEntry("", "", "")
    legend.SetTextSize(0.05)
    legend.AddEntry(hardestVertex, "Hardest Vertex:", "le")
    legend.SetTextSize(0.04)
    legend.AddEntry("", "mean = {} GeV".format(round(hardestVertex.GetMean(), 2)), "")
    legend.Draw("SAME")

    # Add diff mean label
    text1 = ROOT.TLatex()
    text1.SetNDC()
    text1.SetTextFont(42)
    text1.SetTextSize(0.04)
    text1.DrawLatex(0.62, 0.58, "Diff mean = {} MeV".format(round(diff_mean, 2)))
    
    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.05)
    text.DrawLatex(0.18, 0.88, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.18 + 0.13, 0.88, "Internal")
    text.SetTextSize(0.04)
    text.DrawLatex(0.18, 0.82, "X #rightarrow SH #rightarrow b#bar{b}#gamma#gamma")
    text.DrawLatex(0.18, 0.76, extraLabelProcess)
    text.DrawLatex(0.18, 0.70, extraLabelCampaign)
    text.SetTextFont(52)
    text.DrawLatex(0.18, 0.64, selectionLabel)
    
    # Save the plot
    c.SaveAs(path_plots + "{}.{}".format(fig_title, fig_format))
    c.Close()

    # Run the event loop
    ratioPVs = hists["myy_RatioPVs_inv_mass"]

    ratioPVs_n_events = ratioPVs.Integral()

    # Scale histograms to fraction of events
    ratioPVs.Scale(1/ratioPVs_n_events)

    # Create canvas
    c1 = ROOT.TCanvas("c1", "", 700, 750)

    ratioPVs.SetLineColor(ROOT.kBlue+1)
    maxHisto = ratioPVs.GetMaximum()
    heightY = 1.75*maxHisto/ROOT.gPad.GetUymax()
    lastbin = ratioPVs.GetNbinsX()
    overflowBin = lastbin + 1
    ratioPVs.SetBinContent(lastbin, ratioPVs.GetBinContent(lastbin) + ratioPVs.GetBinContent(overflowBin))
    # underflowBin = -1
    # ratioPVs.SetBinContent(0, ratioPVs.GetBinContent(0) + ratioPVs.GetBinContent(underflowBin))
    ratioPVs.SetMinimum(0)
    ratioPVs.SetMaximum(heightY)
    ratioPVs.SetLineStyle(1)
    ratioPVs.SetLineWidth(2)
    ratioPVs.SetFillColor(ROOT.kBlue+1)
    ratioPVs.SetFillStyle(3002)
    #ratioPVs.GetXaxis().SetLabelOffset(999)
    ratioPVs.GetXaxis().SetTitle(xlabel2)
    ratioPVs.GetXaxis().SetLabelFont(43)
    ratioPVs.GetXaxis().SetLabelSize(20)
    ratioPVs.GetXaxis().SetTitleFont(43)
    ratioPVs.GetXaxis().SetTitleSize(25)
    ratioPVs.GetYaxis().SetLabelFont(43)
    ratioPVs.GetYaxis().SetLabelSize(20)
    ratioPVs.GetYaxis().SetTitleFont(43)
    ratioPVs.GetYaxis().SetTitleSize(25)
    ratioPVs.SetMarkerStyle(1)
    ratioPVs.Draw("E1 HIST")
    
    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.05)
    text.DrawLatex(0.18, 0.88, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.18 + 0.18, 0.88, "Internal")
    text.SetTextSize(0.04)
    text.DrawLatex(0.18, 0.82, "X #rightarrow SH #rightarrow b#bar{b}#gamma#gamma")
    text.DrawLatex(0.18, 0.76, extraLabelProcess)
    text.DrawLatex(0.18, 0.70, extraLabelCampaign)
    text.SetTextFont(52)
    text.DrawLatex(0.18, 0.64, selectionLabel)
    
    # Save the plot
    c1.SaveAs(path_plots + "{} Ratio plot.{}".format(fig_title, fig_format))
    c1.Close()

## Function to plot validation plots of Easyjet vs MxAOD
def plotValPlots(hists, n_bins, xlabel, xmin, xmax, extraLabelProcess, extraLabelCampaign, selectionLabel, path_plots, fig_title, fig_format):
    # Run the event loop
    Easyjet = hists["Easyjet"].GetValue()
    MxAOD = hists["MxAOD"].GetValue()

    if 'Fraction' in Easyjet.GetYaxis().GetTitle():
        Easyjet_n_events = Easyjet.Integral()
        MxAOD_n_events = MxAOD.Integral()

        # Scale histograms to fraction of events
        Easyjet.Scale(1/Easyjet_n_events)
        MxAOD.Scale(1/MxAOD_n_events)

    # Set styles
    ROOT.gROOT.SetStyle("ATLAS")

    # Create canvas with pads for main plot and ratio
    c = ROOT.TCanvas("c", "", 700, 750)

    upper_pad = ROOT.TPad("upper_pad", "", 0, 0.3, 1, 1)
    lower_pad = ROOT.TPad("lower_pad", "", 0, 0, 1, 0.3)
    for p in [upper_pad, lower_pad]:
        p.SetLeftMargin(0.14)
        p.SetRightMargin(0.05)
        p.SetTickx(True)
        p.SetTicky(True)
    upper_pad.SetBottomMargin(0.02)
    lower_pad.SetTopMargin(0)
    lower_pad.SetBottomMargin(0.3)

    upper_pad.Draw()
    lower_pad.Draw()

    # Draw histograms
    upper_pad.cd()

    Easyjet.SetLineColor(ROOT.kBlue+1)
    maxHisto = Easyjet.GetMaximum()
    heightY = 1.75*maxHisto/ROOT.gPad.GetUymax()
    lastbin = Easyjet.GetNbinsX()
    overflowBin = lastbin + 1
    Easyjet.SetBinContent(lastbin, Easyjet.GetBinContent(lastbin) + Easyjet.GetBinContent(overflowBin))
    # underflowBin = -1
    # Easyjet.SetBinContent(0, Easyjet.GetBinContent(0) + Easyjet.GetBinContent(underflowBin))
    Easyjet.SetMinimum(0)
    Easyjet.SetMaximum(heightY)
    Easyjet.SetLineStyle(1)
    Easyjet.SetLineWidth(2)
    Easyjet.GetXaxis().SetLabelOffset(999)
    Easyjet.SetMarkerStyle(1)
    Easyjet.Draw("E1 HIST")

    #print("Easyjet histo mean: ", Easyjet.GetMean())

    MxAOD.SetLineColor(ROOT.kOrange-3)
    lastbin = MxAOD.GetNbinsX()
    overflowBin = lastbin + 1
    MxAOD.SetBinContent(lastbin, MxAOD.GetBinContent(lastbin) + MxAOD.GetBinContent(overflowBin))
    # underflowBin = -1
    # MxAOD.SetBinContent(0, MxAOD.GetBinContent(0) + MxAOD.GetBinContent(underflowBin))
    MxAOD.SetLineStyle(1)
    MxAOD.SetLineWidth(2)
    MxAOD.GetXaxis().SetLabelOffset(999)
    MxAOD.SetMarkerStyle(1)
    MxAOD.Draw("E1 AH SAME")
    
    #print("MxAOD histo mean: ", MxAOD.GetMean())

    # Draw ratio
    lower_pad.cd()

    ratio_ref = ROOT.TF1("one", "1", xmin, xmax)
    ratio_ref.SetLineColor(ROOT.kBlack)
    ratio_ref.SetLineStyle(2)
    ratio_ref.SetLineWidth(2)
    ratio_ref.SetMinimum(0)
    ratio_ref.SetMaximum(2)
    ratio_ref.GetXaxis().SetLabelSize(0.08)
    ratio_ref.GetXaxis().SetTitleSize(0.12)
    ratio_ref.GetXaxis().SetTitleOffset(1.0)
    ratio_ref.GetYaxis().SetLabelSize(0.08)
    ratio_ref.GetYaxis().SetTitleSize(0.09)
    ratio_ref.GetYaxis().SetTitle("#frac{Easyjet}{MxAOD}")
    ratio_ref.GetYaxis().CenterTitle()
    ratio_ref.GetYaxis().SetTitleOffset(0.7)
    #ratio_ref.GetYaxis().SetNdivisions(503, False)
    ratio_ref.GetYaxis().ChangeLabel(-1, -1, 0)
    ratio_ref.GetXaxis().SetTitle("{}".format(xlabel))
    ratio_ref.Draw()

    ratio = ROOT.TGraphErrors(n_bins)
    for bin in range(n_bins):
        Easyjet_bin = Easyjet.GetBinContent(bin)
        MxAOD_bin = MxAOD.GetBinContent(bin)
        x_value = hists["Easyjet"].GetXaxis().GetBinCenter(bin)
        if Easyjet_bin == 0.0:
            ratio.SetPoint(bin, x_value, 0)
            ratio.SetPointError(bin, 0, 0)
            continue
        if MxAOD_bin == 0.0: 
            ratio.SetPoint(bin, x_value, -99)
            ratio.SetPointError(bin, 0, 0)
            continue
        ratio_value = Easyjet_bin/MxAOD_bin
        Easyjet_error = Easyjet.GetBinError(bin)
        Easyjet_rel_error = Easyjet_error/Easyjet_bin
        MxAOD_error = MxAOD.GetBinError(bin)
        MxAOD_rel_error = MxAOD_error/MxAOD_bin
        ratio_error = ratio_value * math.sqrt(Easyjet_rel_error**2 + MxAOD_rel_error**2)
        ratio.SetPoint(bin, x_value, ratio_value)
        ratio.SetPointError(bin, 0, ratio_error)

    ratio.SetMarkerStyle(20)
    ratio.SetMarkerSize(1)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetLineWidth(2)
    ratio.Draw("SAME P")

    # Add legend
    upper_pad.cd()
    legend = ROOT.TLegend(0.7, 0.65, 0.9, 0.9)
    legend.SetTextFont(42)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.05)
    legend.SetTextAlign(12)
    legend.AddEntry(Easyjet, "Easyjet" ,"le")
    legend.AddEntry(MxAOD, "MxAOD", "le")
    legend.Draw("SAME")

    # diff_mean = (MxAOD.GetMean() - Easyjet.GetMean())*1E3

    # # Add legend with mean values
    # upper_pad.cd()
    # legend = ROOT.TLegend(0.53, 0.65, 0.9, 0.9)
    # legend.SetTextFont(42)
    # legend.SetFillStyle(0)
    # legend.SetBorderSize(0)
    # legend.SetTextSize(0.05)
    # legend.SetTextAlign(12)
    # legend.AddEntry(Easyjet, "Easyjet:" ,"le")
    # legend.SetTextSize(0.04)
    # legend.AddEntry("", "mean = {} GeV".format(round(Easyjet.GetMean(), 2)), "")
    # legend.AddEntry("", "", "")
    # legend.SetTextSize(0.05)
    # legend.AddEntry(MxAOD, "MxAOD:", "le")
    # legend.SetTextSize(0.04)
    # legend.AddEntry("", "mean = {} GeV".format(round(MxAOD.GetMean(), 2)), "")
    # legend.Draw("SAME")
    # # Add diff mean label
    # text = ROOT.TLatex()
    # text.SetNDC()
    # text.SetTextFont(42)
    # text.SetTextSize(0.04)
    # text.DrawLatex(0.62, 0.58, "Diff mean = {} MeV".format(round(diff_mean, 2)))
    
    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.05)
    text.DrawLatex(0.18, 0.88, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.18 + 0.13, 0.88, "Internal")
    text.SetTextSize(0.04)
    text.DrawLatex(0.18, 0.82, "X #rightarrow SH #rightarrow b#bar{b}#gamma#gamma")
    text.DrawLatex(0.18, 0.76, extraLabelProcess)
    text.DrawLatex(0.18, 0.70, extraLabelCampaign)
    text.SetTextFont(52)
    text.DrawLatex(0.18, 0.64, selectionLabel)
    
    # Save the plot
    c.SaveAs(path_plots + "{}.{}".format(fig_title, fig_format))
    c.Close()

## Function to plot Data/MC plots
def plotDataMCPlots(hists, n_bins, xlabel, ylabel, xmin, xmax, selection, path_subDir_plots, fig_title, fig_format):
    plotInfoDict = SampleDataMCDict()
    
    data = hists["data"]
    # HH = hists["HH"]
    # Hyy = hists["Hyy"]
    yy_jets = hists['yy_jets']
    #ttyy = hists['ttyy']
    #Zqq_yy = hists['Zqq_yy'].GetValue()

    # Set styles
    ROOT.gROOT.SetStyle("ATLAS")

    # Create canvas with pads for main plot and ratio
    c = ROOT.TCanvas("c", "", 700, 750)

    upper_pad = ROOT.TPad("upper_pad", "", 0, 0.3, 1, 1)
    lower_pad = ROOT.TPad("lower_pad", "", 0, 0, 1, 0.3)
    for p in [upper_pad, lower_pad]:
        p.SetLeftMargin(0.14)
        p.SetRightMargin(0.05)
        p.SetTickx(True)
        p.SetTicky(True)
    upper_pad.SetBottomMargin(0.02)
    lower_pad.SetTopMargin(0)
    lower_pad.SetBottomMargin(0.3)

    upper_pad.Draw()
    lower_pad.Draw()

    # Draw histograms
    upper_pad.cd()
    
    stack = ROOT.THStack("", "")    #[HH, Hyy, yy_jets, ttyy], ['HH', 'Hyy', 'yy_jets', 'ttyy']
    #for h, h_name in zip([HH, Hyy, ttyy, yy_jets], ['HH', 'Hyy', 'ttyy', 'yy_jets']):
    for h, h_name in zip([yy_jets], ['yy_jets']):
        h.GetXaxis().SetLabelOffset(999)
        #h.SetLineWidth(2)
        colors = ROOT.gROOT.GetColor(1)
        h.SetFillColor(colors.GetColor(*plotInfoDict[h_name]['color']))
        stack.Add(h)

    stack.Draw("HIST")

    maxStack = data.GetMaximum()
    heightY = 2*maxStack/ROOT.gPad.GetUymax()
    stack.SetMaximum(heightY)
    stack.GetYaxis().SetTitle(ylabel)
    stack.GetYaxis().SetTitleOffset(1.4)
    stack.GetXaxis().SetLabelOffset(999)

    # Draw data histogram
    data.SetMarkerStyle(20)
    data.SetMarkerSize(1.2)
    data.SetLineWidth(2)
    data.SetLineColor(ROOT.kBlack)
    data.Draw("SAME E1")

    # print(data.GetMaximum())
    # print(data.GetMinimum())

    # Draw ratio
    lower_pad.cd()

    ratio_ref = ROOT.TF1("one", "1", xmin, xmax)
    ratio_ref.SetLineColor(ROOT.kBlack)
    ratio_ref.SetLineStyle(2)
    ratio_ref.SetLineWidth(2)
    ratio_ref.SetMinimum(0)
    ratio_ref.SetMaximum(2)
    ratio_ref.GetXaxis().SetLabelSize(0.08)
    ratio_ref.GetXaxis().SetTitleSize(0.12)
    ratio_ref.GetXaxis().SetTitleOffset(1.0)
    ratio_ref.GetYaxis().SetLabelSize(0.08)
    ratio_ref.GetYaxis().SetTitleSize(0.09)
    ratio_ref.GetYaxis().SetTitle("Data/Bkg")
    ratio_ref.GetYaxis().CenterTitle()
    ratio_ref.GetYaxis().SetTitleOffset(0.7)
    #ratio_ref.GetYaxis().SetNdivisions(503, False)
    ratio_ref.GetYaxis().ChangeLabel(-1, -1, 0)
    ratio_ref.GetXaxis().SetTitle("{}".format(xlabel))
    ratio_ref.Draw()

    ratioHist = data.Clone("ratioHist")
    ratioHist.Divide(stack.GetStack().Last())
    ratioHist.SetMarkerColor(ROOT.kBlack)
    ratioHist.SetMarkerStyle(ROOT.kFullDotLarge)
    ratioHist.SetLineColor(ROOT.kBlack)
    ratioHist.SetLineWidth(2)

    # draw line at 1 
    line = ROOT.TLine(ratioHist.GetXaxis().GetXmin(), 1, ratioHist.GetXaxis().GetXmax(), 1)
    line.SetLineColor(ROOT.kBlack)
    line.SetLineStyle(2)
    line.SetLineWidth(1)
    
    ratioHist.SetMaximum(2)
    ratioHist.SetMinimum(0)
    ratioHist.Draw("SAME E1")    
    line.Draw("SAME")

    # Add legend
    upper_pad.cd()
    legend = ROOT.TLegend(0.65, 0.62, 0.9, 0.92)
    legend.SetTextFont(42)
    legend.SetFillStyle(0)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.05)
    legend.SetTextAlign(12)
    legend.AddEntry(data, plotInfoDict["data"]["legend description"], "lep")
    legend.AddEntry(yy_jets, plotInfoDict["yy_jets"]["legend description"], "f")
    # legend.AddEntry(ttyy, plotInfoDict["ttyy"]["legend description"], "f")
    # legend.AddEntry(Hyy, plotInfoDict["Hyy"]["legend description"], "f")
    # legend.AddEntry(HH, plotInfoDict["HH"]["legend description"], "f")
    legend.Draw("SAME")
    
    # Add ATLAS label
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextFont(72)
    text.SetTextSize(0.05)
    text.DrawLatex(0.18, 0.88, "ATLAS")
    text.SetTextFont(42)
    text.DrawLatex(0.18 + 0.13, 0.88, "Internal")
    text.SetTextSize(0.04)
    text.DrawLatex(0.18, 0.82, "#sqrt{#it{s}} = 13 TeV, 140 fb^{-1}")
    text.DrawLatex(0.18, 0.76, "X #rightarrow SH #rightarrow b#bar{b}#gamma#gamma")
    text.SetTextFont(52)
    text.DrawLatex(0.18, 0.70, selection)
    
    # Save the plot
    c.SaveAs(path_subDir_plots + "{} w yy+jets.{}".format(fig_title, fig_format))
    c.Close()